function acc = testClassifier(intervalsPerSegment)
	TRAINPROP = 0.20;

	[vars, labels] = loadDataSets(...
		{'healthyDataSet', 'healthy2DataSet', 'afibDataSet'},...
		intervalsPerSegment);

    popMean = mean(vars, 1);
    popStd = std(vars, [], 1);
    vars = standardize(vars, popMean, popStd);

	[trainingVars, trainingLabels, testVars, testLabels] = ...
		randomSplit(vars, labels, TRAINPROP);

	[featureVec, margins] = trainClassifier(trainingVars, trainingLabels);
	predictedLabels = runClassifier(testVars, featureVec, margins);

	acc = printClassifierStats;

	% figure
	% plotClassDistribution(...
	% 	transformVars(standardize(testVars), featureVec),...
	% 	testLabels, predictedLabels, [1, 2, 3]);

	function acc = printClassifierStats
		tp = sum(testLabels & predictedLabels);
		fp = sum(~testLabels & predictedLabels);
		tn = sum(~testLabels & ~predictedLabels);
		fn = sum(testLabels & ~predictedLabels);
		acc = (tp + tn) / (tp + fp + tn + fn);
	end
end

function [vars, labels] = loadDataSets(paths, intervalLength)
	dataPath = appendToOutPath('watchpat/physiodatasets/');
	count = 0;
	for path = paths
		path = [path{1}, num2str(intervalLength), '.mat'];
		fullPath = [dataPath, path];
		checkForDataSet(path, dataPath);
		if count == 0
			load(fullPath)
		else
			tmp = load(fullPath);
			vars = [vars; tmp.vars];
			labels = [labels, tmp.labels];
		end
	end
end

function checkForDataSet(f, dataPath)
	msg = ['Missing ', f, 'in ', dataPath];
	assert(exist([dataPath, f], 'file') == 2, msg)
end

function [trainingVars, trainingLabels, testVars, testLabels] = ...
	randomSplit(vars, labels, trainingProportion)

	numVars = length(vars);
	idx = randperm(numVars, round(trainingProportion * numVars));
	trainingIdx = false(1, numVars);
	trainingIdx(idx) = true;

	trainingVars = vars(trainingIdx, :);
	trainingLabels = labels(trainingIdx);
	testVars = vars(~trainingIdx, :);
	testLabels = labels(~trainingIdx);
end
