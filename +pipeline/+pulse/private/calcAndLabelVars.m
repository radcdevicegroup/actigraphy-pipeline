function [vars, labels] = calcAndLabelVars(intervalsPerSegment, varargin)
	% Parameters should be in pairs of a database name and a list of file
	% names included in that database.

	vars = [];
	labels = [];
	for paramNum = 1:2:length(varargin)
		database = varargin{paramNum};
		files = varargin{paramNum + 1};

		[setVars, setLabels] = calcVarsForAll(files, database, intervalsPerSegment);

		vars = [vars; setVars];
		labels = [labels; setLabels'];
	end
end

function [vars, labels] = calcVarsForAll(files, database, intervalsPerSegment)
	APPROXNUMVARS = round(150000 * length(files) / intervalsPerSegment);
	vars = nan(APPROXNUMVARS, 13);
	labels = strings(1, APPROXNUMVARS);
	idx = 0;
	for file = files
        try
            record = getRecord(file{1}, database);
            [recordVars, recordLabels] = process(record, intervalsPerSegment);

            numVars = size(recordVars, 1);
            vars((idx + 1):(idx + numVars), :) = recordVars;
            labels((idx + 1):(idx + numVars)) = recordLabels;
            idx = idx + numVars;
        catch
            continue
        end
	end
	vars = vars(~any(isnan(vars), 2), :);
	labels = labels(~strcmp(labels, ""));
end

function [vars, labels] = process(record, intervalsPerSegment)
    [intervals, labels] = stripUnknownsAndRelabel(record.intervals, record.labels);
    vars = segmentAndCalcVars(intervals, intervalsPerSegment);
    [isNormal, isAFib] = labelSegments(labels, vars, intervalsPerSegment);
	[vars, labels] = removeTransitions(vars, isNormal, isAFib);
end

function [intervals, labels] = stripUnknownsAndRelabel(intervals, labels)
	labels = strip(labels);

	isNormal = labels == 'N';
	isAFib = labels == 'AFIB';
	isKnown = isNormal | isAFib;

	intervals = intervals(isKnown);
	labels = labels(isKnown);
end

function [isNormal, isAFib] = labelSegments(labels, vars, intervalsPerSegment)
	numSegments = size(vars, 1);
	labels = labels(1:(intervalsPerSegment * numSegments));
	foldedLabels = reshape(labels, [intervalsPerSegment numSegments]);

	isNormal = sum(strcmp(foldedLabels, 'N'), 1) == intervalsPerSegment;
	isAFib = sum(strcmp(foldedLabels, 'AFIB'), 1) == intervalsPerSegment;
end

function [vars, labels] = removeTransitions(vars, isNormal, isAFib)
	isTransition = ~isNormal & ~isAFib;
	vars = vars(~isTransition, :);
	labels = isAFib(~isTransition);
end
