function plotClassDistribution(vars, trueLabels, predictedLabels, varargin)
	idx = [9, 10, 11];

	if ~isempty(varargin)
		idx = unwrap(varargin);
	end

	afib = vars(trueLabels, :);
	normal = vars(~trueLabels, :);

	ax1 = subplot(121);
	plotAfibVsNormal(afib, normal, idx)
	title('Actual Labels')

	afib = vars(predictedLabels, :);
	normal = vars(~predictedLabels, :);

	ax2 = subplot(122);
	plotAfibVsNormal(afib, normal, idx)
	title('Derived Labels')

	linkaxes([ax1, ax2])
	hlink = linkprop([ax1, ax2], 'View');
	rotate3d on

	function idx = unwrap(args)
		errmsg = 'Index parameter must contain exactly 3 dimensions.';

		if length(args) == 1
			idx = args{1};
		elseif length(args) == 3
			idx = [args{1}, args{2}, args{3}];
		else
			error(errmsg);
		end

		assert(length(idx) == 3, errmsg);
	end

	function plotAfibVsNormal(afib, normal, idx)
		hold on
		plot3(normal(:, idx(1)), normal(:, idx(2)), normal(:, idx(3)), 'g.')
		plot3(afib(:, idx(1)), afib(:, idx(2)), afib(:, idx(3)), 'r.')
		hold off

		legend({'normal', 'afib'})
	end
end
