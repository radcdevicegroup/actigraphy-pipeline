function vars = standardize(vars, popMean, popStd)
	vars = (vars - popMean) ./ popStd;
end
