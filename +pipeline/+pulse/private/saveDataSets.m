function saveDataSets
	intLengths = [32, 64, 128, 256, 512, 1024];
	dataPath = appendToOutPath('watchpat/physiodatasets/');

	healthyFiles = listFileNames('nsrdb');
	healthy2Files = listFileNames('nsr2db');
	afibFiles = listFileNames('ltafdb');

	for l = intLengths
		l
		[vars, labels] = calcAndLabelVars(...
			l,...
			'nsrdb', healthyFiles);
		save([dataPath, 'healthyDataSet', num2str(l), '.mat'], 'vars', 'labels')

		[vars, labels] = calcAndLabelVars(...
			l,...
			'nsr2db', healthy2Files);
		save([dataPath, 'healthy2DataSet', num2str(l), '.mat'], 'vars', 'labels')

		[vars, labels] = calcAndLabelVars(...
			l,...
			'ltafdb', afibFiles(1:40));
		save([dataPath, 'afibDataSet', num2str(l), '.mat'], 'vars', 'labels')
	end
end
