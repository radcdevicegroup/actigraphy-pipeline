function [vars, names] = segmentAndCalcVars(intervals, segmentWidth)
	SPECTRALSAMPFREQ = 4;

	intervals = segment(intervals, segmentWidth);
	vars = calcVars(intervals, SPECTRALSAMPFREQ);
	[vars, names] = struct2mat(vars);
end

function intervals = segment(intervals, width)
	len = nearestMultiple(length(intervals), width);
	intervals = intervals(1:len);
	intervals = reshape(intervals, [width len / width])';

	function out = nearestMultiple(A, B)
		out = floor(A / B) * B;
	end
end

function vars = calcVars(intervals, sampFreq)
	vars = struct();
	vars = calcTimeDomain(intervals, vars);
	vars = calcFreqDomain(intervals, sampFreq, vars);
	vars = calcNonlinear(intervals, vars);

	windowLengths = 4:4:16;
	vars.dfa = calcDfa(intervals, 2, windowLengths);
end

function [vars, fields] = struct2mat(varStruct)
	fields = fieldnames(varStruct);
	minLength = length(varStruct.(fields{1}));
	vars = zeros(minLength, length(fields));
	for fieldNum = 1:length(fields)
		field = fields{fieldNum};
		vars(:, fieldNum) = varStruct.(field);
	end
end
