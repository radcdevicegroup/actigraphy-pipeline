function newVarsQqPlots
	[vars, labels] = loadDataSets(...
		{'healthyDataSet', 'healthy2DataSet', 'afibDataSet'},...
		1024);

    labels = labels == 'true';
    popMean = mean(vars, 1);
    popStd = std(vars, [], 1);
    vars = standardize(vars, popMean, popStd);

	[featureVec, margins] = trainClassifier(vars, labels);
    vars = transformVars(vars, featureVec);

    numSegments = size(vars, 1);
    numVars = size(vars, 2);
    varLabels = repmat(1:numVars, [numSegments, 1]);
    unfold = @(v) reshape(v, [1 (numSegments * numVars)]);
    vars = unfold(vars)';
    varNumbers = unfold(varLabels)';
    labels = repmat(labels, [numVars 1]);

    save('transformedVars', 'vars', 'varNumbers', 'labels')
end

function [vars, labels] = loadDataSets(paths, intervalLength)
	dataPath = appendToOutPath('watchpat/physiodatasets/');
	count = 0;
	for path = paths
		path = [path{1}, num2str(intervalLength), '.mat'];
		fullPath = [dataPath, path];
		checkForDataSet(path, dataPath);
		if count == 0
			load(fullPath)
		else
			tmp = load(fullPath);
			vars = [vars; tmp.vars];
			labels = [labels, tmp.labels];
		end
	end
end

function checkForDataSet(f, dataPath)
	msg = ['Missing ', f, 'in ', dataPath];
	assert(exist([dataPath, f], 'file') == 2, msg)
end
