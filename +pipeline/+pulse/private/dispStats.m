function dispStats(vars, labels, names)
	isNormal = ~labels; isAFib = labels;

	% printStats(...
		% isNormal, 'Normal', ...
		% isAFib, 'AFib');

	close all
	for varNum = 1:size(vars, 2)
		figure
		h = histogram(vars(isNormal, varNum), ...
			'Normalization', 'probability');

		hold on
		histogram(vars(isAFib, varNum), ...
			'Normalization', 'probability', ...
			'BinWidth', h.BinWidth)
		hold off

		legend({'Normal', 'AFib'})
		uiwait
	end

	function printStats(varargin)
		numFields = nargin / 2;
		means = zeros(size(vars, 2), numFields);
		numSegments = zeros(1, numFields);
		for boolNum = 1:2:length(varargin)
			count = (boolNum + 1) / 2;
			indeces = varargin{boolNum};
			means(:, count) = mean(vars(indeces, :), 1);
			numSegments(count) = sum(indeces);
		end

		varNames = strsplit(['Number of segments,', strjoin(names, ',')], ',');
		fieldNames = varargin(2:2:end);
		values = [numSegments; means];

		t = array2table(values);
		t.Properties.VariableNames = fieldNames;
		t.Properties.RowNames = varNames;
		fprintf('\n')
		disp(t)
	end
end
