function featureVec = pca(vars, labels)
	covar = codist(vars, labels);
	featureVec = sortedEig(covar);
end

function V = sortedEig(vals)
	[V, D] = eig(vals);
	[~, idx] = sort(abs(diag(D)), 'descend');
	V = V(:, idx(1:6));
end

function distance = codist(vars, labels)
	afib = labels; normal = ~labels;

	meanA = mean(vars(afib, :), 1);
	meanN = mean(vars(normal, :), 1);
	distA = vars - meanA;
	distN = vars - meanN;

	distance = zeros(size(vars));
	distance(afib, :) = distN(afib, :);
	distance(normal, :) = distA(normal, :);

	distance = (distance' * distance) / (size(distance, 1) - 1);
end
