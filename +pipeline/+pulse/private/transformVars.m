function transformedVars = transformVars(vars, featureVec)
    transformedVars = vars * featureVec;
end
