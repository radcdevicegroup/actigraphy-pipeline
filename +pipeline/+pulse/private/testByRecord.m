function [normCounts, afibCounts] = testByRecord
    classifierPath = appendToOutPath('watchpat/datasets/afibClassifier2.mat');
	classifier = load(classifierPath);

    afibFiles = listFileNames('ltafdb');
    normFiles = listFileNames('nsrdb');
    norm2Files = listFileNames('nsr2db');

    afibCounts = nan(100, 1);
    normCounts = nan(100, 1);
    % afibCounts = [nan, nan];
    % normCounts = [nan, nan];
    ac = 0;
    for f = afibFiles
        ac = ac + 1;
        % afibCounts = [afibCounts; numAfibSegments(f{1}, 'ltafdb')];
        afibCounts(ac) = numAfibSegments(f{1}, 'ltafdb');
    end

    nc = 0;
    for f = normFiles
        nc = nc + 1;
        % normCounts = [normCounts; numAfibSegments(f{1}, 'nsrdb')];
        normCounts(nc) = numAfibSegments(f{1}, 'nsrdb');
    end

    for f = norm2Files
        nc = nc + 1;
        normCounts(nc) = numAfibSegments(f{1}, 'nsr2db');
    end

    afibCounts = afibCounts(~isnan(afibCounts(:, 1)), :);
    normCounts = normCounts(~isnan(normCounts(:, 1)), :);
    figure
    % x = 1:length(afibCounts);
    % afibCounts(:, 2) = afibCounts(:, 2) + 0.1;
    % isAfib = logical(afibCounts(:, 1));
    % bar(x(isAfib), afibCounts(isAfib, 2));
    % hold on
    % bar(x(~isAfib), afibCounts(~isAfib, 2));
    % hold off
    % figure
    % x = 1:length(normCounts);
    % isAfib = logical(normCounts(:, 1));
    % stem(x(isAfib), normCounts(isAfib, 2));
    % hold on
    % stem(x(~isAfib), normCounts(~isAfib, 2));
    % hold off

    plot(nc, rand(1, length(nc)), '.', 'MarkerSize', 14)
    hold on
    plot(ac, rand(1, length(ac)), '.', 'MarkerSize', 14)
    hold off
    legend({'Normal', 'AFib'})

    function count = numAfibSegments(name, database)
        try
            record = getRecord(name, database);
            segmentWidth = classifier.segmentWidth;
            vars = segmentAndCalcVars(record.intervals, segmentWidth);
            isAfib = runClassifier(...
                standardize(vars, classifier.popMean, classifier.popStd),...
                classifier.featureVec,...
                classifier.margins);

            % labels = segment(record.labels, segmentWidth);
            % AFIB = mean(strcmp(labels, "AFIB"), 2);
            % count = [isAfib', AFIB];
            count = sum(isAfib);
        catch
            count = nan;
        end
    end
end

function intervals = segment(intervals, width)
	len = nearestMultiple(length(intervals), width);
	intervals = intervals(1:len);
	intervals = reshape(intervals, [width len / width])';

	function out = nearestMultiple(A, B)
		out = floor(A / B) * B;
	end
end
