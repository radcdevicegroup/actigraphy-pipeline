function createClassifier(segmentWidth)
	[vars, labels] = loadDataSets(...
		{'healthyDataSet2', 'healthy2DataSet2', 'afibDataSet2'},...
		segmentWidth);

    labels = (labels == "true")';
    popMean = mean(vars, 1);
    popStd = std(vars, [], 1);
    vars = standardize(vars, popMean, popStd);

	[featureVec, margins] = trainClassifier(vars, labels);
    classifierPath = appendToOutPath('watchpat/datasets/afibClassifier2.mat');
    save(classifierPath, 'popMean', 'popStd', 'featureVec', 'margins', 'segmentWidth');
end

function [vars, labels] = loadDataSets(paths, intervalLength)
	dataPath = appendToOutPath('watchpat/physiodatasets/');
	count = 0;
	for path = paths
		path = [path{1}, num2str(intervalLength), '.mat'];
		fullPath = [dataPath, path];
		checkForDataSet(path, dataPath);
		if count == 0
			load(fullPath)
		else
			tmp = load(fullPath);
			vars = [vars; tmp.vars];
			labels = [labels, tmp.labels];
		end
	end
end

function checkForDataSet(f, dataPath)
	msg = ['Missing ', f, 'in ', dataPath];
	assert(exist([dataPath, f], 'file') == 2, msg)
end
