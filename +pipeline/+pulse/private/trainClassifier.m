function [featureVec, margins] = trainClassifier(vars, labels)
	featureVec = pca(vars, labels);
	transformedVars = transformVars(vars, featureVec);

	margins = charcterizeClasses(transformedVars, labels);

	function margins = charcterizeClasses(vars, labels)
		margins.aCov = cov(vars(labels, :));
		margins.nCov = cov(vars(~labels, :));
		margins.aMean = mean(vars(labels, :), 1);
		margins.nMean = mean(vars(~labels, :), 1);
	end
end
