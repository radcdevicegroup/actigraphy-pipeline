function out = apen(intervals)
% Approximate Entropy (ApEn) as described in "Physiological time-series
% analysis what does regularity quantify?" by Steven M. Pingus And Ary
% L. Goldberger.
%
% m is the number of values to include in each grouping.
% r is the max distance between close groups.

    m = 2;
    r = 0.6;

    numSegments = size(intervals, 1);
    allHeartRates = 1 ./ intervals;
    out = zeros(numSegments, 1);

    for segmentNum = 1:numSegments
        heartRates = allHeartRates(segmentNum, :);
        farVals = findFarVals(heartRates, r);

        numCloseGroupsM = calcNumCloseGroups(heartRates, m);
        numCloseGroupsMPlus1 = calcNumCloseGroups(heartRates, m + 1);
        numCloseGroupsMPlus2 = calcNumCloseGroups(heartRates, m + 2);

        Cm = numCloseGroupsMPlus1 / numCloseGroupsM;
        CmPlus1 = numCloseGroupsMPlus2 / numCloseGroupsMPlus1;

        phi = @(C) mean(log(C));
        out(segmentNum) = phi(CmPlus1) - phi(Cm);
    end

    function farVals = findFarVals(heartRates, r)
        distMat = calcDistMatrix(heartRates);
        farVals = abs(distMat) > r;
    end

    function mat = calcDistMatrix(heartRates)
        squareHeartRates = repmat(heartRates, [length(heartRates) 1]);
        mat = squareHeartRates - squareHeartRates';
    end

    function numGroups = calcNumCloseGroups(heartRates, m)
        kernal = eye(m);
        closeGroups = conv2(farVals, kernal, 'valid') == 0;
        numGroups = sum(sum(closeGroups));
    end
end
