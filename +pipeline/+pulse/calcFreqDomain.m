function [psa, spen, lle] = calcFreqDomain(intervals, sampFreq)
	% Calculates frequency domain heart rate variables.
	%
	% Provides: power spectral analysis.

	psa = zeros(size(intervals, 1), 1);
	spen = zeros(size(intervals, 1), 1);
	lle = zeros(size(intervals, 1), 1);
	for segmentNum = 1:size(intervals, 1)
		segment = unifySampling(intervals(segmentNum, :), sampFreq);
		powerSpec = abs(fft(segment, [], 2) .^ 2);
		psa(segmentNum) = calcPsa(powerSpec);
		spen(segmentNum) = calcSpen(powerSpec);
		lle(segmentNum) = calcLle(segment);
	end

	function intervals = unifySampling(intervals, sampFreq)
		time = cumsum(intervals);
		intervals = resample(intervals, time, sampFreq, 'spline');
	end

	function psa = calcPsa(powerSpec)
		% The ratio of low frequency (0.04 - 0.15Hz) components to high
		% frequency (0.15 - 0.4Hz) components of n-n intervals.

		freqs = linspace(0, sampFreq, size(powerSpec, 2));
		LF = sum(powerSpec(indexBetween(freqs, 0.04, 0.15)));
		HF = sum(powerSpec(indexBetween(freqs, 0.15, 0.4)));

		psa = LF / HF;

		function range = indexBetween(freqs, lower, upper)
			range = freqs > lower & freqs < upper;
		end
	end

	function spen = calcSpen(powerSpec)
		powerSpec = powerSpec(:, 1:round(end/2));
		probs = powerSpec ./ sum(powerSpec, 2);

		spen = - sum(probs .* log2(probs), 2);
	end

	function lle = calcLle(segment)
		RADIUS = 0.01;
		TIMERANGE = 10:40;
		NUMTESTS = 20;

		startingPoints = randperm(length(segment), NUMTESTS);
		slope = zeros(1, NUMTESTS);
		testNum = 0;
		for startPoint = startingPoints
			testNum = testNum + 1;
			closePoints = pointsWithinRange(segment(startPoint));

			if length(closePoints) < 2
				continue
			end

			samplePoints = closePoints + TIMERANGE';
			logDistance = log(calcDistanceFromStart(samplePoints));

			bs = [TIMERANGE', ones(length(TIMERANGE), 1)] \ logDistance;
			slope(testNum) = bs(1);
		end
		lle = mean(slope);

		function closePoints = pointsWithinRange(point)
			range = 1:length(segment);
			closePoints = segment < (point + RADIUS) & segment > (point - RADIUS);
			closePoints = closePoints & range < range(end - TIMERANGE(end));
			closePoints = range(closePoints);
		end

		function distance = calcDistanceFromStart(points)
			distanceFromStart = abs(segment(points) - segment(startPoint));
			distance = mean(distanceFromStart, 2);
		end
	end
end
