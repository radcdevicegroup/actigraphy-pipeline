function labels = runClassifier(vars, classifier)
	standardize(vars, classifier.popMean, classifier.popStd);
	transformedVars = transformVars(vars, classifier.featureVec);
	labels = qda(transformedVars, classifier.margins);
end

function labels = qda(vars, margins)
	probA = norm(margins.aMean, margins.aCov, vars);
	probN = norm(margins.nMean, margins.nCov, vars);

	labels = probA > probN;
end

function n = norm(m, v, x)
	x = x - m;
	e = -0.5 * sum((x * inv(v)) .* x, 2)';
	detV = prod(diag(2 * pi * chol(v)));

	n = exp(e) / sqrt(detV);
end
