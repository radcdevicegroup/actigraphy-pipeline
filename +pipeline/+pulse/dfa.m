function [alpha, yInt] = dfa(intervals, degree, windowLengths)
% Performs detrended fluctuation analysis on a signal of heart beat
% intervals. Returning time asleep, and percent of time in different
% sleep phases.
%
% Based off "Comparison of Detrended Fluctuation analysis and spectral
% analysis for heart rate variability in sleep and sleep apnea"

	numWindows = length(windowLengths);
	numSegments = size(intervals, 1);
	profile = calcProfile(intervals);

	ff = zeros(numSegments, numWindows);
	for windowNum = 1:numWindows
		windowLength = windowLengths(windowNum);
		for segmentNum = 1:numSegments
			segmentProfile = profile(segmentNum, :);
			ff(segmentNum, windowNum) = ...
				calcFluctuationFunction(segmentProfile, windowLength, degree);
		end
	end

	[slope, yInt] = calcLinearRegression(ff, windowLengths);
	alpha = slope;
end

function profile = calcProfile(intervals)
	numIntervals = size(intervals, 2);
	profile = movsum(intervals, [numIntervals 0], 2);
end

function ff = calcFluctuationFunction(profile, windowLength, degree)
	windows = windowBy(windowLength, profile);
	polynomialFit = leastSquares(windows, degree);
	ffSquared = calcVariance(windows, polynomialFit);
	ff = rms(ffSquared);

	function windows = windowBy(windowLength, profile)
		numIntervals = length(profile);
		numwindows = floor(numIntervals / windowLength);
		numIntervals = windowLength * numwindows;
		trimmedProfileRight = profile(1:numIntervals);
		trimmedProfileLeft = profile(end:-1:(end - numIntervals + 1));

		windows = [reshape(trimmedProfileRight, [windowLength numwindows])';
				   reshape(trimmedProfileLeft, [windowLength numwindows])'];
	end

	function p = leastSquares(windows, degree)
		powers = (0:degree)';
		x = 1:(size(windows, 2));
		poly = x .^ powers;

		coefficients = poly' \ windows';
		p = coefficients' * poly;
	end

	function variance = calcVariance(windows, p)
		variance = mean((windows - p) .^ 2, 1);
	end

	function out = rms(squareSignal)
		out = sqrt(mean(squareSignal));
	end
end

function [slope, yInt] = calcLinearRegression(ff, windowLengths)
	logff = log10(ff)';
	logsl = log10(windowLengths)';
	logsl = [ones(length(logsl), 1), logsl];
	betas = logsl \ logff;
	yInt = betas(1, :)';
	slope = betas(2, :)';
end
