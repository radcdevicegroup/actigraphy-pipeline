function out = csi(intervals, differences)
% From Poincar ́e Plots in Analysis of Selected Biomedical Signals Agnieszka
% Kitlas Golińska

    [sd1, sd2] = calcSd();
    [sd1, sd2] = removeZeros(sd1, sd2);
    out = mean(sd2 ./ sd1, 2);

    function [sd1, sd2] = calcSd()
        sd1 = (sqrt(2) / 2) * std(differences, [], 2);
        sd2 = sqrt(2 * (std(intervals, [], 2) .^ 2) - ...
                   (std(differences, [], 2) .^ 2) / 2);
    end

    function [sd1, sd2] = removeZeros(sd1, sd2)
        iszero = sd1 == 0;
        sd1 = sd1(~iszero);
        sd2 = sd2(~iszero);
	end
end
