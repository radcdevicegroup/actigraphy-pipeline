function [intervals, intervalTimes] = getIntervals(pulse);
	MAXBPM = 300;

	signal = movmean(pulse.data, 10);
	mpp = mean(abs(signal)) * 0.25;
	mpd = pulse.fs * 60 / MAXBPM;

	[~, locs] = findpeaks(signal, ...
		'MinPeakProminence', mpp, ...
		'MinPeakDistance', mpd);

	time = linspace(1, length(pulse) / pulse.fs, length(pulse))';
	intervalTimes = time(locs);
	intervals = diff(intervalTimes);
	intervalTimes = intervalTimes(2:end);
end
