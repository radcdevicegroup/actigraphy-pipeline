function calculateAll(record, classifierPath)
%CALCULATEALL calculate pulse variables
%   CALCULATEALL(RECORD) calculate pulse variables for a RECORD. RECORD must
%   have a pulse signal. All variables are stored in RECORD's variables
%   struct under the pulse field.
%
%   CALCULATEALL(RECORD, CLASSIFIERPATH) Specify the path to a classifier.
%   Defaults to afibClassifier in the current directory. The classifier
%   selects segment width used so changing the classifier may change the
%   values of the calculated variables.

    import pipeline.pulse.*
    if nargin == 1
        d = fileparts(which('pipeline.pulse.calculateAll'));
        classifierPath = utils.pathjoin(d, 'afibClassifier.mat');
    end
    classifier = load(classifierPath);

    pulse = record.getSignal('pulse');
    addVariable = @(name, value) record.addVariable('pulse', name, value);

    [intervals, times] = getIntervals(pulse);
    intervals = segment(intervals, classifier.segmentWidth);
	differences = diff(intervals, [], 2);

    addVariable('time_since_start', times(classifier.segmentWidth:classifier.segmentWidth:end));

    %% Time-domain
    addVariable('mean', mean(intervals, 2));
    addVariable('sdnn', std(intervals, [], 2));
    addVariable('sdsd', std(differences, [], 2));
    addVariable('pnn25', pnn(differences, 0.025));
    addVariable('pnn50', pnn(differences, 0.05));

    %% Freq-domain
    uniformSamplingFreq = 4;
    [psa, spen, lle] = calcFreqDomain(intervals, uniformSamplingFreq);
    addVariable('psa', psa);
    addVariable('spen', spen);
    addVariable('lle', lle);

    %% Non-linear
    addVariable('csi', csi(intervals, differences));
    addVariable('apen', apen(intervals));
    [posposdensity, negnegdensity] = sta(differences);
    addVariable('posposdensity', posposdensity);
    addVariable('negnegdensity', negnegdensity);

	windowLengths = 4:4:16;
	addVariable('dfa', dfa(intervals, 2, windowLengths));

    %% Afib classification
    isAfib = runClassifier(struct2double(record.variables.pulse), ...
                           classifier);
    addVariable('isAfib', isAfib);
end

function intervals = segment(intervals, width)
	len = nearestMultiple(length(intervals), width);
	intervals = intervals(1:len);
	intervals = reshape(intervals, [width len / width])';

	function out = nearestMultiple(A, B)
		out = floor(A / B) * B;
	end
end

function [vars, fields] = struct2double(varStruct)
	fields = fieldnames(varStruct);
    fields = fields(~strcmpi(fields, 'time_since_start'));
	minLength = length(varStruct.(fields{1}));
	vars = zeros(minLength, length(fields));
	for fieldNum = 1:length(fields)
		field = fields{fieldNum};
		vars(:, fieldNum) = varStruct.(field);
	end
end
