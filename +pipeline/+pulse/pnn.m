function out = pnn(differences, minTime)
% Percent successive n-n intervals that differ by more than minTime
% (in seconds).

    largeValues = abs(differences) > minTime;
    totalValues = size(differences, 2);
    out = sum(largeValues, 2) / totalValues;
end
