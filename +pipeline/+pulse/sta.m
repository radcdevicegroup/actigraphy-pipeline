function [posposdensity, negnegdensity] = sta(differences)
    direction = sign(differences);
    sequentialDiffs = direction(:, 2:end) + direction(:, 1:end-1);
    pospos = sequentialDiffs == 2;
    negneg = sequentialDiffs == -2;

    total = size(sequentialDiffs, 2);
    posposdensity = sum(pospos, 2) ./ total;
    negnegdensity = sum(negneg, 2) ./ total;
end
