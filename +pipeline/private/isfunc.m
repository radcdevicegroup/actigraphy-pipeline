function TF = isfunc(x)
    TF = (isa(x, 'function_handle')) || (strcmp('None'));
end
