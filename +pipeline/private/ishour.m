function TF = ishour(x)
%ISHOUR test if x is an integer between [0 and 24)
    TF = (utils.isnaturalnumber(x) && x < 24) || (strcmpi('None', x));
end
