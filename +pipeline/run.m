function run(files, outDir, varargin)
%RUN send files through the pipeline and save the output
%   RUN(FILES, OUTDIR) read, calculate, and print variables for all files in
%   FILES. If a file has already been processed it is skipped (see below for
%   overriding this behavior). Information for each record is added to a
%   records file and device information is added to a devices file (once for
%   each device even if used in multiple records) variables are printed to
%   different files based on the variables sets they belong to (See
%   DEVICE/RECORD/PRINT).
%
%   Output files are saved to OUTDIR within the format [prefix variableSet]. All
%   information about the pipeline's history is saved in OUTDIR, if a pipeline
%   is run with a new OUTDIR or different PREFIX (see below) the pipeline will
%   run through all FILES even if they had been processed in another OUTDIR (or
%   PREFIX).
%
%   RUN(DIR, OUTDIR) if RUN is passed a directory name it will be converted to a
%   list of files then behave just as above. The first argument is always sent
%   to UTILS/LISTALLFILES so argument 1 can be anything that LISTALLFILES can
%   convert to a list including a list of directories, globs, and a mix of
%   directories, files, and globs.
%
%   RUN(..., 'DELIM', DELIM) change the delimiter used to separate fields in the
%   output files.
%   RUN(..., 'OVERWRITE', TRUE) delete previous output data from the OUTDIR and
%   PREFIX combination and run all files as if none had been previously
%   processed.
%   RUN(..., 'PREFIX', PREFIX) change the prefix used for naming the output
%   files. Defaults to 'pipeline_'. Using different prefixes allows multiple
%   pipeline to be saved to the same OUTDIR.
%   RUN(..., 'SIGNALS', SIGNALLIST) pass SIGNALLIST to READER (See READ).
%   RUN(..., 'STARTOFDAY', STARTOFDAY) pass STARTOFDAY to READER (See READ).
%   RUN(..., 'MINIMUMDAYS', MINDAYS) Assert the record is at least MINDAYS long.
%   Set to 'NONE' to prevent check.
%   RUN(..., 'NAMEREGEXP', NAMEREGEXP) pass NAMEREGEXP to READER (See READ).
%
%   RUN(..., 'READER', READER) use function READER instead of default
%   PIPELINE.READ to read in files. READER should needs to be accept a file
%   path, a signals list and start of day time in a similar manner to READ and
%   return a DEVICE/RECORD object.
%
%   RUN(..., 'CALCULATOR', CALCULATOR) use function CALCULATOR to calculate
%   variables instead of default PIPELINE.CALCULATEVARIABLES. Must accept a
%   record and add variables to it's variable structure. Variables should be
%   added via RECORD.ADDVARIABLE sse CALCULATEVARIABLES for an example.
%
%   RUN(..., 'DEBUG', TRUE) turn on debugging. When DEBUG is true pass debug
%   parameter to DEVICE/MAP causing the pipeline to halt at an error instead
%   of writing a summary of the error to a log file.
%
%   See also READ, CALCULATEVARIABLES, UTILS/LISTALLFILES, DEVICE/RECORD/PRINT.

    defaultStartOfDay = 19;
    defaultMinDays = 2;
    defaultReader = @pipeline.read;
    defaultCalculator = @pipeline.calculateVariables;

    p = inputParser;
    p.addParameter('delim', ',', @ischar);
    p.addParameter('overWrite', false, @islogical);
    p.addParameter('prefix', 'pipeline', @isstr);
    p.addParameter('signals', {}, @iscellstr);
    p.addParameter('startOfDay', defaultStartOfDay, @ishour);
    p.addParameter('minimumDays', defaultMinDays, @(x) (utils.isnaturalnumber(x) || strcmp('None', x)));
    p.addParameter('nameRegexp', '', @iscellstr);
    p.addParameter('reader', defaultReader, @isfunc);
    p.addParameter('calculator', defaultCalculator, @isfunc);
    p.addParameter('debug', false, @islogical);
    p.parse(varargin{:});

    files = utils.listFiles(files);

    if ~exist(outDir, 'dir')
        mkdir(outDir);
    end

    if p.Results.overWrite
        allPipelineFiles = vertcat(utils.listFiles(generateFullPath('*')), ...
                                   utils.listFiles(generateFullPath('*', true), ...
                                                   'showHidden', true));
        if ~isempty(allPipelineFiles)
            for f = allPipelineFiles'
                delete(f{1});
            end
        end
    end

    processedFile = generateFullPath('processed', true);
    if isfile(processedFile)
        % May not scale up well.
        fid = fopen(processedFile, 'r');
        processedFiles = textscan(fid, '%s\n');
        fclose(fid);

        files = files(~ismember(cellfun(@basename, files, 'UniformOutput', false), ...
                                 processedFiles{:}));
        clear('processedFiles');
    end

    if isempty(files)
        return
    end

    read = p.Results.reader;
    readVariables = suppliedVariables(p, {'startOfDay', 'signals', 'nameRegexp'});
    calculateVariables = p.Results.calculator;

    fileIds.processed = fopen(processedFile, 'a');
    device.map(@pipe, files, 'log', logpath, 'debug', p.Results.debug);
    closeFiles(fileIds);
    updateDevices;

    function vars = suppliedVariables(p, names)
    % If name-value parameter explicitly supplied add to list of variables
    % otherwise do nothing.

        vars = {};
        for name = names
            if ~contains(name{1}, p.UsingDefaults)
                vars = {vars{:}, name{1}, p.Results.(name{1})};
            end
        end
    end

    function pipe(file)
        record = read(file, readVariables{:});

        if ~(strcmpi(p.Results.minimumDays, 'None') || strcmpi(record.startOfDay, 'None'))
            assert(record.getNumDays >= p.Results.minimumDays, ...
                   'Pipeline:ShortRecord', ...
                   'Record saved to %s is less than minimum number of days long', file);
        end

        calculateVariables(record);
        print(record);
        fprintf(fileIds.processed, '%s\n', basename(file));
    end

    function print(record)
        openFiles = fieldnames(fileIds);
        variableSets = record.listVariableSets';
        additionalArgs = {};
        for variableSet = {'records', 'devices', variableSets{:}}
            if ~ismember(variableSet{1}, openFiles)
                filename = generateFullPath(variableSet{1});

                if ~isfile(filename)
                    additionalArgs = {'header', true};
                end

                fileIds.(variableSet{1}) = ...
                    fopen(generateFullPath(variableSet{1}), 'a');
            end

            record.print(fileIds.(variableSet{1}), variableSet{1}, ...
                         'delim', p.Results.delim, additionalArgs{:});
        end
    end

    function base = basename(file)
        [~, f, ext] = fileparts(file);
        base = [f, ext];
    end

    function path = generateFullPath(file, ishidden)
        if nargin == 1 || ~ishidden
            dot = '';
        else
            dot = '.';
        end

        prefix = p.Results.prefix;
        if ~(isempty(file) || isempty(prefix) || endsWith(prefix, '_'))
            prefix = [prefix '_'];
        end

        path = utils.pathjoin(outDir, [dot prefix file '.csv']);
    end

    function path = logpath
        path = generateFullPath('');
        [d, f] = fileparts(path);
        f = [f '.log'];
        path = utils.pathjoin(d, f);
    end

    function closeFiles(ids)
        for file = fieldnames(ids)'
            fclose(ids.(file{1}));
        end
    end

    function updateDevices()
        devicePath = generateFullPath('devices');
        if ~isfile(devicePath)
            return
        end

        deviceTable = unique(readtable(devicePath));
        writetable(deviceTable, devicePath, 'Delimiter', p.Results.delim);
    end
end
