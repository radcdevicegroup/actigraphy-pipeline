function indices = accelerometer(record)
    featureSet = 'qcFeatures';
    pipeline.qcFunctions.calcFeatures(record, featureSet);
    features = variableSet2Double(record, featureSet);

    qcDir = fileparts(which('pipeline.qcFunctions.accelerometer'));
    modelParams = load(utils.pathjoin(qcDir, 'private/accModel.mat'));

    features = features(:, modelParams.featuresToKeep(1:(end - 1)));
    features = features * modelParams.pcaProjectionMatrix;
    features = features(:, modelParams.pcaFeaturesToKeep);
    features = [ones(size(features, 1), 1), features];
    indices = modelParams.model(features) == 1;

    % Clean up so feature vector not printed in pipeline.
    record.removeVariableSet(featureSet);

    function mat = variableSet2Double(record, variableSet)
        variables = record.listVariables(variableSet);
        variables = variables(~strcmpi('day', variables));
        mat = zeros(length(record.variables.(variableSet).day), length(variables));
        for i = 1:length(variables)
            mat(:, i) = record.variables.(variableSet).(variables{i});
        end
    end
end
