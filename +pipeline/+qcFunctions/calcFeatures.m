function calcFeatures(record, featureSet)
    acc = record.getSignal('accelerometer');
    acc.reshape;
    enmo = acc.norm - 1;
    addVariable = @(name, value) record.addVariable(featureSet, name, value);

    addVariable('day', 1:acc.getNumDays);
    addVariable('mean', mean(enmo, 1));
    addVariable('meanX', mean(acc.data(:, :, 1), 1));
    addVariable('meanY', mean(acc.data(:, :, 2), 1));
    addVariable('meanZ', mean(acc.data(:, :, 3), 1));
    addVariable('sd', std(enmo, [], 1));
    addVariable('min', min(enmo, [], 1));
    addVariable('max', max(enmo, [], 1));
    addVariable('median', median(enmo, 1));

    sorted = sort(enmo, 1);
    addVariable('perc25', median(enmo(1:floor(length(sorted)/2), :), 1));
    addVariable('perc75', median(enmo(floor(length(sorted)/2):end, :), 1));
    clear('sorted');

    addVariable('skew', stats.skew(enmo, 1));
    addVariable('kertosis', stats.kertosis(enmo, 1));
    addVariable('meanDiff', mean((enmo(3:end, :) - enmo(1:end-2, :)) * acc.fs / 2, 1));

    addVariable('corrXY', corr(acc.data(:, :, 1), acc.data(:, :, 2)));
    addVariable('corrXZ', corr(acc.data(:, :, 1), acc.data(:, :, 3)));
    addVariable('corrYZ', corr(acc.data(:, :, 2), acc.data(:, :, 3)));

    power = abs(fft(enmo, [], 1) .^ 2);
    power = power(2:floor(length(power) / 2), :);
    hzTicks = linspace(1, length(power), acc.fs / 2);
    for hz = 1:19
        hzIndex = round(hzTicks(hz)):round(hzTicks(hz+1));
        avgPower = mean(power(hzIndex, :), 1);

        if hz > 1
            addVariable(['powerChange' num2str(hz-1)], ...
                        (prev - avgPower) ./ prev);
        end

        addVariable(['power' num2str(hz)], avgPower);
        prev = avgPower;
    end
    acc.reshape('original');

    function r2 = corr(x, y)
        x = x - mean(x, 1);
        y = y - mean(y, 1);
        
        normalizationFactor = (sum(x .^ 2, 1) .^ 0.5) .* (sum(y .^ 2, 1) .^ 0.5);
        r2 = sum(x .* y, 1) ./ normalizationFactor;
    end
end
