function record = read(file, varargin)
%READ get a record from a file
%   RECORD = READ(FILE) construct a record from a device generated data FILE.
%   RECORD = READ(..., 'SAVE', true) save the record to a mat file.
%   RECORD = READ(..., 'NAMEREGEXP', PATTERN) Use regex PATTERN as the file
%   naming convention.
%   RECORD = READ(..., 'STARTOFDAY', STARTOFDAY) pass STARTOFDAY to record constructor.
%   RECORD = READ(..., 'QCFUNC', QCFUNC) pass QCFUNC to record constructor.
%   Default QCFUNC depends on the device type.
%   RECORD = READ(..., 'SIGNALS', SIGNALLIST) specify the desired signals. If
%   a record does not have a file in SIGNALLIST return an error. If the
%   record has extra signals, those signals are stripped from the record
%   before returning. This prevents CALCULATEVARIABLES from calculating additional
%   variable sets.
%
%   Support for new devices can be added by telling DETECTDEVICE how to
%   identify the new devices files and including the device to the read switch
%   statement. If the devices files use a different naming convention also
%   add a regex pattern to the KNOWNFILENAMEREGEXES variable.
%
%   See also DEVICE/RECORD/RECORD, CALCULATEVARIABLES, REGEXP.

    import device.Record device.Signal;
    import pipeline.readers.*;

    %Use for parseFileName
    knownFileNameRegexes = ...
        {'^(?<projId>\d{8})_(?<fuYear>\d{2})_(?<cycle>\d{1})_(?<deviceId>\d{1,4}$)'
         ['^(?<projId>\d{8})-(?<fuYear>\d{2})-\d*_(left|right) wrist_' ...
          '(?<deviceId>\d{6})_\d{4}-\d{2}-\d{2} \d{2}-\d{2}-\d{2}$']
         '^(?<projId>\d{8})-(?<fuYear>\d{2})$'};

    defaultQcFunc = 'None';
    defaultStartOfDay = 19;
    isfunc = @(f) isa(f, 'function_handle') || strcmpi(f, 'None');

    p = inputParser;
    p.addParameter('save', false, @islogical);
    p.addParameter('startOfDay', defaultStartOfDay, @ishour);
    p.addParameter('nameRegexp', knownFileNameRegexes, @iscellstr);
    p.addParameter('qcFunc', defaultQcFunc, isfunc);
    p.addParameter('signals', {}, @iscellstr);
    p.parse(varargin{:});

    [keys.projId, keys.fuYear, keys.deviceId] = ...
        parseFileName(file, p.Results.nameRegexp);

    qcFunc = [];
    startOfDay = [];
    deviceType = detectDevice(file);
    switch deviceType
      case 'Actical'
        assertSignals({'counts'});
        [counts, fs, info.startTime, info.endTime, deviceId] = readActical(file);
        if ~isempty(deviceId) && isempty(keys.deviceId)
            keys.deviceId = deviceId;
        end

        signals.counts = Signal(counts, fs);
        info.placement = 'wrist';
      case {'GENEActiv', 'AX3'}
        assertSignals({'accelerometer'})
        if strcmp(deviceType, 'GENEActiv')
            [acc, fs, info.startTime, info.endTime, keys.deviceId] = ...
                readGeneactiv(file);
        else
            [acc, fs, info.startTime, info.endTime] = readAx3(file);
        end

        signals.accelerometer = Signal(acc, fs);
        info.placement = 'wrist';
        qcFunc = 'None'; % TODO replace with accelerometer qc function.
      case 'WatchPAT'
        assertSignals({'pulse'})
        [pulse, fs, info.startTime, info.endTime, keys.deviceId] = ...
            readWatchpat(file);
        signals.pulse = Signal(pulse, fs);
        info.placement = 'wrist';
        startOfDay = 'None';
      case 'Mat file'
        record = device.load(file);
        assertSignals(record.listSignals);
        removeUnwantedSignals(record);
        assert(isa(record, 'device.Record'), 'Pipeline:Reader:NotARecordFile', ...
               'The file %s does not contain a Record object.', file);
        return
    end

    if ~contains('qcFunc', p.UsingDefaults) || isempty(qcFunc)
        qcFunc = p.Results.qcFunc;
    end

    if ~contains('startOfDay', p.UsingDefaults) || isempty(startOfDay)
        startOfDay = p.Results.startOfDay;
    end

    additionalArgs = {'startOfDay', startOfDay, 'qcFunc', qcFunc};
    record = Record(keys, info, deviceType, signals, additionalArgs{:});
    removeUnwantedSignals(record);
    
    %1st 7 Days
%     fprintf("\nNOTE: Using First 7 Days of Wear Only\n");
%     t2 = record.info.startTime + caldays(7);
%     slice(record, record.info.startTime, t2);
    %End
   
    if p.Results.save
        savePath = regexprep(file, '\..*?$', '.mat');
        record.save(savePath);
    end

    function assertSignals(signalList)
        if isempty(p.Results.signals)
            return
        end

        if contains('accelerometer', signalList) && ~contains('counts', signalList)
            signalList = {signalList{:}, 'counts'};
        end

        desiredSignalsStr = join(p.Results.signals, ', ');
        assert(all(contains(p.Results.signals, signalList)), ...
               'Pipeline:Reader:MissingSignal', ...
               'Device type can not generate all signals in: %s.', desiredSignalsStr{1});
    end

    function removeUnwantedSignals(record)
        if isempty(p.Results.signals)
            return
        end

        existingSignals = record.listSignals;
        wantedSignals = p.Results.signals;

        if ismember('counts', wantedSignals) && ~ismember('counts', existingSignals)
            pipeline.actical.accelerometer2Counts(record);
        end

        signalsToRemove = ~ismember(existingSignals, wantedSignals);
        if sum(signalsToRemove) > 0
            record.removeSignal(existingSignals(signalsToRemove));
        end
    end
end
