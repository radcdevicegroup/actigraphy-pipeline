function calculateVariables(record)
%CALCULATEVARIABLES default variables to calculate in pipeline
%   CALCULATEVARIABLES(RECORD) tests RECORD's signals and info to determine
%   which variables to calculate. Variables are added to RECORD's variables
%   structure under unique variable sets.
%
%   See also DEVICE/RECORD/LISTALLVARIABLESETS, DEVICE/RECORD/ADDVARIABLE.

    if record.hasSignal('counts') || record.hasSignal('accelerometer')        
        pipeline.actical.calculateAll(record);
    end

    if record.hasSignal('accelerometer')               
        pipeline.accelerometer.calculateAll(record);
    end

    if record.hasSignal('pulse')
        pipeline.pulse.calculateAll(record);
    end
end
