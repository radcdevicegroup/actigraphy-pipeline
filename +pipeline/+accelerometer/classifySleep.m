function [avgHrsSleep, varSleep] = classifySleep(acc)
%CLASSIFYSLEEP find time spent sleeping
%   [AVGHRSSLEEP, VARSLEEP] = CLASSIFYSLEEP(ACC) using the accelerometer
%   signal ACC find periods of sleep and return the average number of hours
%   per day spent sleeping and the standard deviation sleep time.

    acc.norm;
%     acc.resample(1);
    acc.reshape('days');

    dailyHrsSleep = calcSleep(acc);
    avgHrsSleep = mean(dailyHrsSleep);
    varSleep = std(dailyHrsSleep);

    function sleep = calcSleep(acc)
        power = @(s) abs(s) .^ 2;
        dbs = @(power) 10 * log10(power ./ mean(power, 1));

        sleep = zeros(acc.getNumDays, 1);
        for n = 1:acc.getNumDays
            sleep(n) = calcDailySleep(acc(:, n), dbs, power);
        end
    end

    function daysSleep = calcDailySleep(signal, dbs, power)
        isSleep = zeros(1, 24);
        time = 1:24;
        window = 3600 * acc.fs;

        for hrs = [1, 3, 6]
            [s, ~, t] = spectrogram(signal, window * hrs, 0, [], acc.fs);
            t = t / 3600;
            isSleep = isSleep + ...
                      interp1(t, mean(dbs(power(s)), 1), time, 'nearest', 'extrap');
        end
        daysSleep = sum(isSleep < mean(isSleep));
	end
end
