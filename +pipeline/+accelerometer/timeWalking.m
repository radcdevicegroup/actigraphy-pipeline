function minutes = timeWalking(steps)
%TIMEWALKING calculate minutes spent walking
%   MINUTES = TIMEWALKING(STEPS) given an array STEPS of times (in seconds)
%   of steps, returns the total MINUTES spent walking.
%   Periods of walking are defined as windows when at least 20 steps were
%   taken every 5 minutes.
%
%   See also DETECTSTEPS.

    seconds = findWalkingTime(steps);
    minutes = seconds / 60;

    function time = findWalkingTime(steps)
        PERIOD = 5 * 60;
        MINSTEPSPERPERIOD = 20;
        steps = ceil(steps);
        stepIdx = false(1, steps(end));
        stepIdx(steps) = true;

        locs = movsum(stepIdx, PERIOD) > MINSTEPSPERPERIOD;
        locs = movmean(locs, 5 * 60) > 0;
        locs = movmean(locs, 1 * 60) > 0;

        % The zero at the beginning ensures a start transition if already
        % walking at the start.
        % The zero at end ensures a stop transition at the end if walking at end.
        transitions = diff([0 locs 0]);
        startOfWalking = find(transitions > 0);
        endOfWalking = find(transitions < 0);

        time = sum(endOfWalking - startOfWalking);
    end
end
