function [steps, frequencies, amplitudes] = detectSteps(acc)
%DETECTSTEPS find when steps are taken
%   STEPS = DETECTSTEPS(ACC) using the accelerometer signal ACC, find the
%   times points (in seconds) when steps were taken.
%
%   [STEPS, FREQUENCIES, AMPLITUDES] = DETECTSTEPS(ACC) also calculate the
%   frequency of steps and the relative amplitude of the arm swing for each step.

	MAXSTEPSPERSECOND = 3;
	MINSTEPSPERSECOND = 1;

	acc.norm;
	acc.reshape('days');
	time = acc.getTime;

	maxNumSteps = ceil(time(end, end) * MAXSTEPSPERSECOND);
	stepIndices = zeros(1, maxNumSteps) - 1;
	frequencies = stepIndices;
	idx = 1;
	for day = 1:acc.getNumDays
		position = doubleIntegral(acc(:, day), acc.fs);
		peakLocs = findUpToDownPeaks(position);
		[locs, frequencies] = ...
			findCyclicalLocs(time(:, day), position, peakLocs, acc.fs);
		stepIndices(idx:(idx + length(locs) - 1)) = locs + ((day - 1) * length(acc));
		frequencies(idx:(idx + length(frequencies) - 1)) = frequencies;
		idx = idx + length(locs);
	end

	stepIndices = stepIndices(stepIndices > 0);
	frequencies = frequencies(frequencies > 0);
	steps = time(stepIndices);
	amplitudes = diff(acc(stepIndices));

	function position = doubleIntegral(acceleration, sampFreq)
		velocity = integrate(acceleration, sampFreq);
		position = integrate(velocity, sampFreq);
	end

	function integral = integrate(signal, sampFreq)
		signal = signal - movmean(signal, 4 * sampFreq, 1);
		integral = cumsum(signal, 1) / sampFreq;
	end

	function locs = findUpToDownPeaks(signal)
		minPeakHeight = mean(abs(signal)) * 0.8;
		minPeakDistance = 0;

		[~, locs] = findpeaks(abs(signal), ...
			'MinPeakProminence', minPeakHeight, ...
			'MinPeakDistance', minPeakDistance...
		);

		dSig = diff(signal);
		locs = locs([diff(dSig(locs - 1) > 0) ~= 0; false]);
	end

	function [locs, freqs] = findCyclicalLocs(time, signal, peakLocs, sampFreq)
		assert(length(peakLocs) > 0, 'No peaks found.');
		[freqSignal, rawFreq, peakLocs] = ...
			makeFreqSignal(length(time), peakLocs, sampFreq);

		kernal = makeCorrelationKernal(time, freqSignal);
		amp = interp1(peakLocs, signal(peakLocs), (1:length(time))', 'spline', 'extrap');
		corr = compareToCyclicSignal(signal, kernal, sampFreq, amp);
		locs = findPeaksInCylclicAreas(peakLocs, corr, rawFreq);
		freqs = rawFreq(locs) * sampFreq;
	end

	function [freqs, raw, peakLocs] = makeFreqSignal(sigLength, peakLocs, sampFreq)
		freqs = 1 ./ (2 * diff(peakLocs));
		freqs = interp1(peakLocs(1:end-1), freqs, (1:sigLength)', 'spline', 'extrap');
		raw = 4 * sampFreq * freqs;
		freqs = cumsum(freqs);
	end

	function kernal = makeCorrelationKernal(time, freqs)
		kernal = exp(1i * 2 * pi * freqs);
	end

	function corr = compareToCyclicSignal(signal, kernal, sampFreq, amp)
		corr = signal .* kernal;
		smoothingWindow = hann(sampFreq * 10);
		corr = conv(corr, smoothingWindow, 'same') ./ ...
			   conv(abs(amp), smoothingWindow, 'same');

		corr = abs(corr .^ 2);
		corr = movmean(corr, sampFreq * 10);
	end

	function stepLocs = findPeaksInCylclicAreas(peakLocs, corr, freq)
		stepWindows = corr > 0.1;
		peakLocs = peakLocs(freq(peakLocs) > MINSTEPSPERSECOND & ...
							freq(peakLocs) < MAXSTEPSPERSECOND);
		stepLocs = peakLocs(stepWindows(peakLocs));
	end
end
