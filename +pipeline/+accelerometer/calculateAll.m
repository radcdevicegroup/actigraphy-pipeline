function calculateAll(record)
%CALCULATEALL add accelerometer variables to a record
%   CALCULATEALL(RECORD) calculate variables using a RECORD's accelerometer
%   signal and save them to the signal's variable property.
%
%   See also +ACCELEROMETER/, ++DEVICE/RECORD, +DEVICE/SIGNAL.

	assert(record.hasSignal('accelerometer'), ...
		   'Pipeline:MissingSignal', ...
		   'Record does not have an accelerometer signal.');

	import pipeline.accelerometer.*

	acc = record.getSignal('accelerometer');    
        
	addVariable = @(varname, value) record.addVariable('accelerometer', varname, value);

	assert(acc.getNumDays > 0, 'Pipeline:ShortRecording','Recording too short.')

	[estimatedSteps, stepFrequencies, stepAmplitudes] = detectSteps(acc.copy);

	addVariable('cadence_avg', mean(stepFrequencies));
	addVariable('cadence_var', std(stepFrequencies));
	addVariable('arm_swing', mean(abs(stepAmplitudes)));
	addVariable('time_walking_avg', ...
				timeWalking(estimatedSteps) / acc.getNumDays);

	[avgHrsSleep, varsSleep] = classifySleep(acc.copy);

	addVariable('sleep_avg', avgHrsSleep);
	addVariable('sleep_var', varsSleep);
end
