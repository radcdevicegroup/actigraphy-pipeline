function [alpha, yInt] = detrendedFluctuationAnalysis(counts, degree, windowLengths)
%DETRENDEDFLUCTUATIONANALYSIS detrended fluctuation analysis (DFA)
%   ALPHA = DETRENDEDFLUCTUATIONANALYSIS(COUNTS, DEGREE, WINDOWLENGTHS)
%   Claculated the power law exponent ALPHA of COUNTS using a polynomial of
%   DEGREE and WINDOWLENGTHS (in samples).
%
%   [ALPHA, YINT] = DETRENDEDFLUCTUATIONANALYSIS(COUNTS, DEGREE, WINDOWLENGTHS)
%   In addition to ALPHA as above return the YINT of the power law.
%
%   References:
%       Comparison of Detrended Fluctuation analysis and spectral
%       analysis for heart rate variability in sleep and sleep apnea
%
%       Noninvasive fractal biomarker of clock neurotransmitter disturbance
%       in humans with dementia

	numWindows = length(windowLengths);
	counts = counts - mean(counts);
	profile = calcProfile(counts);

	ff = zeros(numWindows, 1);
	for windowNum = 1:numWindows
		windowLength = windowLengths(windowNum);
		ff(windowNum) = ...
			fluctuationFunction(profile, windowLength, degree);
	end

	[alpha, yInt] = linearRegression(ff, windowLengths);
end

function profile = calcProfile(counts)
	numIntervals = size(counts, 1);
	profile = movsum(counts, [numIntervals 0], 1);
end

function ff = fluctuationFunction(profile, windowLength, degree)
	windows = windowBy(windowLength, profile);
	polynomialFit = leastSquares(windows, degree);
	ffSquared = calcVariance(windows, polynomialFit);
	ff = rms(ffSquared);

	function windows = windowBy(windowLength, profile)
		numIntervals = length(profile);
		numwindows = floor(numIntervals / windowLength);
		numIntervals = windowLength * numwindows;
		trimmedProfileRight = profile(1:numIntervals);
		trimmedProfileLeft = profile(end:-1:(end - numIntervals + 1));

		windows = [reshape(trimmedProfileRight, [windowLength numwindows]), ...
				   reshape(trimmedProfileLeft, [windowLength numwindows])];
	end

	function p = leastSquares(windows, degree)
		powers = 0:degree;
		x = (1:(size(windows, 1)))';
		poly = x .^ powers;

		coefficients = poly \ windows;
		p = poly * coefficients;
	end

	function variance = calcVariance(windows, p)
		variance = mean((windows - p) .^ 2, 2);
	end

	function out = rms(squareSignal)
		out = sqrt(mean(squareSignal));
	end
end

function [slope, yInt] = linearRegression(ff, windowLengths)
	logff = log10(ff);
	logsl = log10(windowLengths);
	logsl = [ones(length(logsl), 1), logsl];
	betas = logsl \ logff;
	yInt = betas(1, :);
	slope = betas(2, :);
end
