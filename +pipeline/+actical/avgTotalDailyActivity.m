function value = avgTotalDailyActivity(counts)
    epochsPerDay = (3600 * 24) * counts.fs;
    numDays = length(counts) / epochsPerDay;

    totalActivity = sum(counts.data, 1);
    value = totalActivity / numDays;
    
    %Divide by 100,000
    value = value/100000;
end
