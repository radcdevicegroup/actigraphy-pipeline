function accelerometer2Counts(record, epochDuration)
%ACCELEROMETER2COUNTS calculate counts from a record's accelerometer signal.
%   ACCELEROMETER2COUNTS(RECORD) add counts signal to RECORD's signals.
%   ACCELEROMETER2COUNTS(RECORD, EPOCHDURATION) calculate counts with
%   EPOCHDURATION long epochs instead of default 15 (in seconds).
%
%   If the record already has counts, the original record is returned.
%   Otherwise, convert the accelerometer signal to a count signal similar to
%   that from an Actical record. Throws an error if RECORD is not an actigraphy type.
%
%   Example:
%      import pipeline.read pipeline.actical.accelerometer2Counts;
%      record = read(test.cwa);
%      record.hasSignal('counts'); % Returns false.
%      accelerometer2Counts(record);
%      record.hasSignal('counts'); % Returns true.
%
%   See also READ, +DEVICE/RECORD.

    isActigraphy = @(rec) (rec.hasSignal('counts') || rec.hasSignal('accelerometer'));
    assert(isActigraphy(record), 'Record is not an actigraphy type.')
    if record.hasSignal('counts')
        return
    end

    if nargin == 1
        epochDuration = 15;
    end

    validateattributes(epochDuration, {'numeric'}, ...
                       {'positive', 'integer', 'ncols', 1, 'nrows', 1});

    acc = record.getSignal('accelerometer');
    windowLength = epochDuration * acc.fs;
    countsSampleFreq = 1 / epochDuration;
    SCALE = 4;

    counts = acc(:, 2);
    counts = removeOutOfRangeValues(counts);
    counts = filterSignal(counts, acc.fs / 2);
    counts = removeBaseline(counts, acc.fs);
    counts = abs(counts);
    counts = removeSmallValues(counts);
    counts = movsum(counts, windowLength);
    counts = downSample(counts, windowLength, acc.fs) * SCALE;
    record.addSignal('counts', counts, countsSampleFreq, 1);
end

function signal = removeOutOfRangeValues(signal)
    THRESHOLD = 2;
    MINVALUE = 0.05;
    signal(abs(signal) < MINVALUE) = 0;
    signal(signal > THRESHOLD) = THRESHOLD;
    signal(signal < -THRESHOLD) = -THRESHOLD;
end

function signal = filterSignal(signal, nyquistFreq)
    a = 1;
    n = 52;
    freqs = [0 0.001 0.9 3.8 3.9 nyquistFreq];
    b = firls(n, freqs / nyquistFreq, [0 0 1 1 0 0]);
    signal = filtfilt(b, a, signal);
end

function signal = removeBaseline(signal, sampFreq)
    baseline = movmean(signal, sampFreq, 1);
    signal = signal - baseline;
end

function signal = removeSmallValues(signal)
    THRESHOLD = 0.19;
    signal(signal < THRESHOLD) = 0;
end

function signal = downSample(signal, windowLength, sampFreq)
    isFullDays = false;
    samplesPerDay = sampFreq * 3600 * 24;
    if mod(length(signal), samplesPerDay) == 0
        isFullDays = true;
    end

    signal = signal(1:windowLength:end);

    if isFullDays
        newSamplesPerDay = samplesPerDay / windowLength;
        signal = fixSamplingErrors(signal, newSamplesPerDay);
    end

    function signal = fixSamplingErrors(signal, samplesPerDay)
        if mod(length(signal), samplesPerDay) == 0
            return
        end

        numDays = length(signal) / samplesPerDay;
        expectedLength = round(numDays) * samplesPerDay;

        if expectedLength > length(signal)
            diff = expectedLength - length(signal);
            buffer = repmat(signal(end), [diff 1]);
            signal = [signal; buffer];
        else
            signal = signal(1:expectedLength);
        end
    end
end
