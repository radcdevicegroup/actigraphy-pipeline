function [mvpa, numDays] = moderateToVigorousActivity(counts, thresh)
%MVPA average minutes per day with at least THRESH counts

    epochsPerMinute = 60 * counts.fs;

    countsPerMin = convertToMinutes(counts.data, epochsPerMinute);
    atleastModerateActivity = countsPerMin > thresh;

    mvpa = sum(atleastModerateActivity) / counts.getNumDays;

    mvpaByDay = reshape(atleastModerateActivity, [24 * 60 counts.getNumDays])';
    numDays = sum(any(mvpaByDay, 2));

    function cpm = convertToMinutes(data, epochsPerMinute)
        summedCounts = movsum(data, epochsPerMinute);
        cpm = summedCounts((epochsPerMinute - 1):epochsPerMinute:end);
    end
end
