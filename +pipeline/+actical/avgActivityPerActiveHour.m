function value = avgActivityPerActiveHour(counts, restThresh)
    activeEpochs = counts.data > restThresh;
    totalActivity = sum(counts.data(activeEpochs), 1);

    epochsPerHour = counts.fs * 3600;
    numActiveHours = sum(activeEpochs, 1) / epochsPerHour;

    value = totalActivity / numActiveHours;
    
    %Divide by 100,000
    value = value/100000;
end
