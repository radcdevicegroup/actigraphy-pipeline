function value = avgDailyInactivity(counts, restThresh)
    restEpochs = sum(counts.data <= restThresh, 1);   
    totalNumEpochs = length(counts);
    value = restEpochs / totalNumEpochs;
end
