function alpha = fractalRegularity(counts, fs)
%FRACTALREGULARITY temporal regularity of activity
%   DELTAALPHA = FRACTALREGULARITY(COUNTS) perform second degree detrended fluctuation
%   analysis on counts on a time scale between 1.25 and 90 minutes.
%
%   Reference:
%       Fractal regulation and incident Alzheimer's disease in elderly
%       individuals
%
%   See also DETRENDEDFLUCTUATIONANALYSIS

    import actigraphy.acticalVariables.detrendedFluctuationAnalysis

    minutes2samples = @(minutes) fs * 60 * minutes;
    windowLengths = minutes2samples(1.25):minutes2samples(0.25):minutes2samples(90);
    alpha = detrendedFluctuationAnalysis(counts, 2, windowLengths');
end
