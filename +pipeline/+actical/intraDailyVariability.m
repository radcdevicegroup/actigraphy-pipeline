function value = intraDailyVariability(counts)
% IV measure of variance in activity between hours of the day.
    
    %BINARIZED:
    %countsPerHour = sum(counts.reshape('hours') > 0, 1);
    
    %NON-BINARIZED:
    countsPerHour = sum(counts.reshape('hours'), 1);
    
    X_i = countsPerHour;
    X_bar = mean(X_i);

    SSE = @(x, x_bar) mean((x - x_bar) .^2);
    sumSquareDifferences = SSE(X_i(2:end), X_i(1:(end - 1)));
    sumSquareResiduals = SSE(X_i, X_bar);

    value = sumSquareDifferences / sumSquareResiduals;
    
    %Andrew code
    
% function value = intraDailyVariability(vector, binwidth)
%     if sum(vector) > 0
%         if (binwidth == 1)
%             data = vector;
%         else
%             data = bin(vector, binwidth);
%         end
%         l = length(data);
%         for i=2:l
%             squareddiff(i,1) = ((data(i)-data(i-1))^2);
%         end
%         sumdiff=sum(squareddiff(1:l, 1));
%         value=(sumdiff)/((l-1)*(var(data,1)));
%     end
%     
%         function retvar = bin(vector, binwidth)
%             l = length(vector);
%             n = floor(l/binwidth);
%             j=0;
%             for i=1:n
%                 a=(binwidth*i);
%                 b=a-binwidth+1;
%                 j=j+1;
%                 m=sum(vector(b:a));
%                 retvar(j,1)=m;
%             end
%         end
%     
% end
