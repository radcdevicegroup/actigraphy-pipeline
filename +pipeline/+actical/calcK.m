function k = calcK(counts, direction, restThresh, doplot)
    [pTrans, epochNums, N_t] = ...
        calcPropabilityOfTransition(counts, direction, restThresh);

    timesInMinutes = epochNums / (counts.fs * 60);
    regression = stats.lowess(timesInMinutes, pTrans, 0.3);

    constantRegionIndex = findConstantRegion(pTrans, regression);
    weights = (N_t(1:end - 1, :) - N_t(2:end, :)) .^ 0.5;

    weightedMean = @(x, w) dot(x, w) / sum(w, 1);
    k = weightedMean(pTrans(constantRegionIndex), weights(constantRegionIndex));

    if nargin > 3 && strcmpi(doplot, 'plot')
        plot(timesInMinutes, pTrans, '.', 'MarkerSize', 8)
        hold on
        plot(timesInMinutes, regression)
        plot(timesInMinutes(constantRegionIndex), regression(constantRegionIndex))
        hold off
        switch direction
          case 'AR'
            xlabel('Time since last rest epoch (minutes)')
            ylabel('Probability of resting')
          case 'RA'
            xlabel('Time since last movement (minutes)')
            ylabel('Probability of breaking rest')
        end

        legend({'Transitions', 'LOWESS regression', 'Constant region'})
        kstr = sprintf('$k_{%s} = %0.1g$', direction, k);
        strx = get(gca, 'XTick');
        strx = timesInMinutes(constantRegionIndex(end)) + (strx(2) - strx(1)) / 2;
        text(strx, k, kstr)
    end
end

function [pTransition, timesProbs, N_t] = ...
        calcPropabilityOfTransition(counts, transition, restThresh)
    % Calculate probabilty of transitioning from one state to another for
    % all t less than t_max.
    %
    % Parameter transition is 'AR' for active to rest and 'RA' for rest to
    % active.

    if strcmpi(transition, 'AR')
        runs = counts.data > restThresh;
    elseif strcmpi(transition, 'RA')
        runs = counts.data <= restThresh;
    else
        error('Not an allowed value for transition.')
    end

    runDurations = sort(findDurationsOf(runs));
    tMax = runDurations(end);
    N_t = sum(runDurations >= (1:tMax)', 2);
    [d, N_t, times] = calcDistanceBetweenDifferingN(N_t);

    pTransition = (N_t(1:end - 1) - N_t(2:end)) ./ (N_t(1:end - 1) .* d);
    timesProbs = times(1:end - 1);

    function objectLengths = findDurationsOf(objects)
        labeledObjects = bwlabel(objects);
        numObjects = max(labeledObjects);
        objectLengths = sum(labeledObjects == (1:numObjects), 1);
    end

    function [d, N_t, timesOfChange] = calcDistanceBetweenDifferingN(N_t)
    % Finds distance d such that d is the smallest value for N_t ~= N_(t+d).

        times = (1:length(N_t))';
        diffVals = N_t(1:end - 1) ~= N_t(2:end);
        timesOfChange = times([diffVals; true]);
        N_t = N_t(timesOfChange);
        d = diff(timesOfChange);
    end
end

function index = findConstantRegion(pTrans, regression)
    stdev = std(pTrans);
    dRegression = diff(regression);

    varianceRep = repmat(abs(dRegression), [1 length(dRegression)]);
    delays = tril(ones(length(dRegression)));
    delayedVariance = varianceRep .* delays;

    cumVariance = cumsum(delayedVariance, 1);
    smallVariancesRanges = (cumVariance < stdev) .* delays;
    smallVariancesRanges = sum(smallVariancesRanges, 1);
    [len, where] = max(smallVariancesRanges);
    index = where:(where + len);
end
