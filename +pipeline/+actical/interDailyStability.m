function value = interDailyStability(counts)
%IS measure of similarity in activity patterns between days.
%   See Alterations in the Circadian Rest-Activity Rhythm in Aging and
%   Alzheimer's Disease.

    %BINARIZED:
    %countsPerHour = sum(counts.reshape('hours') > 0, 1);
    
    %NON-BINARIZED:
    countsPerHour = sum(counts.reshape('hours'), 1);
    
    countsByDay = reshape(countsPerHour, [24 counts.getNumDays])';

    X_i = countsPerHour;
    X_barH = mean(countsByDay, 1);

    value = var(X_barH) / var(X_i);
end
