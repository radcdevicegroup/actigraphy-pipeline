function vars = calculateAll(record, restThresh)
%CALCULATEALL calculate actical variables for record
%   CALCULATEALL(RECORD) calculate variables from RECORD's counts signal
%   and save them to counts' variables map. If RECORD has an accelerometer signal
%   but no counts signal the counts signal will be generated from the
%   accelerometer signal via ACCELEROMETER2COUNTS. If RECORD has neither signal
%   an error will be thrown.
%
%   CALCULATEALL(RECORD, RESTTHRESH) use RESTTHRESH for determining
%   whether an epoch is rest or active instead of 0.
%
%   See also ACCELEROMETER2COUNTS, +ACTICAL/, +DEVICE/RECORD, +DEVICE/SIGNAL.

    assert(record.hasSignal('counts') || record.hasSignal('accelerometer'), ...
           'Pipeline:MissingSignal', ...
           'Record does not have a counts signal or a precursor signal.')

    import pipeline.actical.*

    if ~record.hasSignal('counts')
        accelerometer2Counts(record);
    end

    if nargin == 1
        restThresh = 0;
    end

    counts = record.getSignal('counts'); 
    
    addVariable = @(varname, value) record.addVariable('actical', varname, value);

    assert(counts.getNumDays > 0, 'Pipeline:ShortRecording','Recording too short.');

    addVariable('tactivity_d_avg', avgTotalDailyActivity(counts));

    addVariable('tactivity_acth_avg', ...
                avgActivityPerActiveHour(counts, restThresh));

    addVariable('pert_noact_avg', ...
                avgDailyInactivity(counts, restThresh));

    addVariable('kar', calcK(counts, 'AR', restThresh));
    addVariable('kra', calcK(counts, 'RA', restThresh));
    addVariable('is', interDailyStability(counts));
    addVariable('iv', intraDailyVariability(counts));
%     vector = counts.data;
%     binwidth = 240;
%     addVariable('iv', intraDailyVariability(vector, binwidth));

    [m10, l5, m9, m8, m7, m6, m5, m4, m3, m2, m1] = max10AndLeast5(counts);
    addVariable('m10', m10);
    addVariable('m9', m9);
    addVariable('m8', m8);
    addVariable('m7', m7);
    addVariable('m6', m6);
    addVariable('m5', m5);
    addVariable('m4', m4);
    addVariable('m3', m3);
    addVariable('m2', m2);
    addVariable('m1', m1);
    addVariable('l5', l5);
    addVariable('amplitude', m10 - l5);
    addVariable('dsr', dailySustainedRest(counts, restThresh));

    MVPATHRESHOLD = 1100;
    [mvpa, daysMvpa] = moderateToVigorousActivity(counts, MVPATHRESHOLD);
    addVariable('mvpa', mvpa);
    addVariable('daysMvpa', daysMvpa);
end
