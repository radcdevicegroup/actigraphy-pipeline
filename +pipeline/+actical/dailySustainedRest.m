function value = dailySustainedRest(counts, restThresh)
%DSR average hours per day of sustained rest.
%   Sustained rest is defined as periods of no activity at least 5 minutes in
%   duration.

    restEpochs = counts.data <= restThresh;
    [numberedRestRuns, numRuns] = bwlabel(restEpochs);
    durationRestRuns = sum(numberedRestRuns == (1:numRuns), 1);

    epochsPerMinute = 60 * counts.fs;
    minRunDuration = 5 * epochsPerMinute;

    durationRestRuns = durationRestRuns(durationRestRuns >= minRunDuration);
    totalSustainedRest = sum(durationRestRuns);

    avgSustainedRestPerDay = totalSustainedRest / counts.getNumDays;
    epochsPerHour = 60 * epochsPerMinute;

    value = avgSustainedRestPerDay / epochsPerHour;
end
