function [m10, l5, m9, m8, m7, m6, m5, m4, m3, m2, m1] = max10AndLeast5(counts)
%M10/L5 max average counts per hour over 10 hours and least over 5 hours.
%   See Alterations in the Circadian Rest-Activity Rhythm in Aging and
%   Alzheimer's Disease.

    countsPerHour = sum(counts.reshape('hours'), 1);
    countsByDay = reshape(countsPerHour, [24 counts.getNumDays]);

    hourlyMeans = mean(countsByDay, 2);
    if length(hourlyMeans) < 5
        l5 = mean(hourlyMeans);
        m10 = l5;
    elseif length(hourlyMeans) < 10
        m10 = mean(hourlyMeans);
        l5 = min(movmean(hourlyMeans, 5, 'Endpoints', 'discard'));
    %m(1,2...10)
    else
        m10 = max(movmean(hourlyMeans, 10, 'Endpoints', 'discard'));
        m9 = max(movmean(hourlyMeans, 9, 'Endpoints', 'discard'));
        m8 = max(movmean(hourlyMeans, 8, 'Endpoints', 'discard'));
        m7 = max(movmean(hourlyMeans, 7, 'Endpoints', 'discard'));
        m6 = max(movmean(hourlyMeans, 6, 'Endpoints', 'discard'));
        m5 = max(movmean(hourlyMeans, 5, 'Endpoints', 'discard'));
        m4 = max(movmean(hourlyMeans, 4, 'Endpoints', 'discard'));
        m3 = max(movmean(hourlyMeans, 3, 'Endpoints', 'discard'));
        m2 = max(movmean(hourlyMeans, 2, 'Endpoints', 'discard'));
        m1 = max(movmean(hourlyMeans, 1, 'Endpoints', 'discard'));
        l5 = min(movmean(hourlyMeans, 5, 'Endpoints', 'discard'));
    end

end
