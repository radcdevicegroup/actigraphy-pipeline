function [acc, fs, startTime, endTime, id] = readGeneactiv(filePath)
	[acc, fs, startTime, id] = readGeneactivBin(utils.pathexpand(filePath));

	startTime = datetime(startTime, 'InputFormat', 'yyyy-MM-dd HH:mm:ss:SSS');
	endTime = seconds(length(acc) / fs) + startTime;
end
