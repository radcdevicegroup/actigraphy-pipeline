function [counts, fs, startTime, endTime, deviceId] = readActical(filepath)
	[~, name, ext] = fileparts(filepath);

	switch lower(ext)
	  case '.txt'
		counts = readTxt(filepath);
		fs = 1 / 15;
		% Pre-processed files already trimmed into days but start time not given.
		endTime = datetime([datestr(datetime('today')) ' 19:00:00']);
		startTime = endTime - ((15 * length(counts)) / (3600 * 24));
		deviceId = '';
	  case '.csv'
		[counts, fs, startTime, endTime, deviceId] = readCsv(filepath);
	  case '.awc' % TODO
		if strcmp(ext, '.awc')
			error('Pipeline:Reader', ...
				  'Actical awc reader has not been setup.');
		end
		counts = readAwc(filepath);
	end
end

function counts = readTxt(filepath)
	counts = csvread(filepath);
end

function [counts, fs, startTime, endTime, deviceId] = readCsv(filepath)
	fid = fopen(filepath, 'r');
	l = '';
	while isempty(regexp(l, '^\d'))
		l = fgetl(fid);
		if contains(l, 'Start Date:')
			startDate = regexp(l, '\d{1,2}-\w{3}-\d{4}', 'match');
		elseif contains(l, 'Start Time:')
			startTime = regexp(l, '\d+:\d+', 'match');
		elseif contains(l, 'Serial Number')
			deviceId = regexp(l, '\w+$', 'match');
			deviceId = deviceId{1};
		end
	end

	fseek(fid, -(length(l) + 2), 0);
	contents = textscan(fid, '%f,%f,%f,%[^,],%[^,],%f,%f,');
	counts = contents{end-1};
	fs = 1 / (contents{3}(2) - contents{3}(1));
	startTime = datetime([startDate{1} ' ' startTime{1}]);
	endTime = datetime([contents{4}{end} ' ' contents{5}{end}]);
	fclose(fid);
end							 

function counts = readAwc(filepath)
	fileStr = fileread(filepath);
	counts = str2double(regexp(fileStr, '\d*(?= ,)', 'match'));
end
