/* Build with:
 * mex -R2018a CFLAGS="\$CFLAGS -fopenmp" LDFLAGS="\$LDFLAGS -fopenmp" -lm
 * readGeneactivBin.c */

#include <math.h>
#include <mex.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define NUMTHREADS 4
#define LINESPERPAGE 11
#define SAMPLESPERPAGE 300
#define HEXCHARSPERSAMPLE 12

static int page = 0;
static int is_corrupt = 0;

static void scan_for_keyword(FILE *file_ptr, const char *key_word, int *key_len,
                             char *data, const int data_len) {
  *key_len = strlen(key_word);
  while (strncmp(data, key_word, *key_len) != 0) {
    fgets(data, data_len, file_ptr);
  }
}

static int str_2_int(const char *str, const int len) {
  char result[len + 1];
  result[len] = '\0';

  return atoi(strncpy(result, str, len));
}

static int read_header(FILE *file_ptr, int *id, int *samp_freq,
                       char *start_time, double *calibration_gain,
                       double *calibration_offset, int *expected_num_pages) {

  const int data_len = 200;
  char data[data_len];
  int str_offset;

  scan_for_keyword(file_ptr, "Device Unique Serial Code", &str_offset, data,
                   data_len);
  *id = str_2_int(data + str_offset + 1, 6);

  scan_for_keyword(file_ptr, "Measurement Frequency", &str_offset, data,
                   data_len);
  *samp_freq = str_2_int(data + str_offset + 1, 2);

  scan_for_keyword(file_ptr, "Start Time", &str_offset, data, data_len);
  strncpy(start_time, data + str_offset + 1, 23);

  scan_for_keyword(file_ptr, "Calibration Data", &str_offset, data, data_len);

  const int gain_str_len = 7, offset_str_len = 9;
  for (int i = 0; i < 3; i++) {
    fgets(data, data_len, file_ptr);
    calibration_gain[i] =
        (double)str_2_int(data + gain_str_len, strlen(data) - gain_str_len);
    fgets(data, data_len, file_ptr);
    calibration_offset[i] =
        (double)str_2_int(data + offset_str_len, strlen(data) - offset_str_len);
  }

  scan_for_keyword(file_ptr, "Number of Pages", &str_offset, data, data_len);
  *expected_num_pages = str_2_int(data + str_offset + 1, data_len);

  return 0;
}

static void load_first_data_stream(FILE *file_ptr, char *data,
                                   const int data_len) {
  int str_offset;

  scan_for_keyword(file_ptr, "Measurement Frequency", &str_offset, data,
                   data_len);
  fgets(data, data_len, file_ptr);
}

static int load_next_data_stream(FILE *file_ptr, char *data,
                                 const int data_len) {
  extern int page;
  int c;
  for (int i = 0; i < LINESPERPAGE; i++) {
    fgets(data, data_len, file_ptr);
  }

  if ((c = fgetc(file_ptr)) == EOF) {
    is_corrupt = 1;
  }

  page += 1;
  return page;
}

static double hex_char_2_double(char hex_char) {
  double value = 0;

  if (hex_char >= '0' && hex_char <= '9') {
    value = hex_char - '0';
  } else if (hex_char >= 'a' && hex_char <= 'f') {
    value = hex_char - 'a' + 10;
  } else if (hex_char >= 'A' && hex_char <= 'F') {
    value = hex_char - 'A' + 10;
  } else {
    exit(3);
    printf("Incorrect hex character.");
  }
  return value;
}

static double hex_2_double(const char *hex_str, const int hex_len) {
  double value = 0;

  for (int i = 0; i < hex_len; i++) {
    value += hex_char_2_double(hex_str[i]) * pow(16, (hex_len - (i + 1)));
  }

  return value;
}

static double sign(double x) {
  if (x > 0)
    return 1.;
  else
    return -1.;
}

static void read_page(const char *data, const int index,
                      const double *calibration_offset,
                      const double *calibration_gain, int num_pages,
                      double *acc) {

  const int hex_chars_per_value = HEXCHARSPERSAMPLE / 4;
  int offset;
  char hex_data[HEXCHARSPERSAMPLE];

  for (int j = 0; j < SAMPLESPERPAGE; j++) {
    for (int k = 0; k < 3; k++) {
      offset = index + j + (k * SAMPLESPERPAGE * num_pages);
      strncpy(hex_data,
              data + (j * HEXCHARSPERSAMPLE) + (k * hex_chars_per_value),
              hex_chars_per_value);

      acc[offset] = pow(2, 11) - hex_2_double(hex_data, hex_chars_per_value);
      acc[offset] = sign(acc[offset]) * (pow(2, 11) - fabs(acc[offset]));
      acc[offset] =
          ((acc[offset] * 100) - calibration_offset[k]) / calibration_gain[k];
    }
  }
}

static int read_bin_data(FILE *file_ptr, const double *calibration_gain,
                         const double *calibration_offset, const int num_pages,
                         double *acc) {

  const int data_len = SAMPLESPERPAGE * HEXCHARSPERSAMPLE + 1;
  char data[data_len];
  int index;
  int p = 0;
  int pages_per_thread;

  load_first_data_stream(file_ptr, data, data_len);
  index = (p * SAMPLESPERPAGE);
  read_page(data, index, calibration_offset, calibration_gain, num_pages, acc);

  while (((num_pages - 1 - p) % NUMTHREADS) != 0) {
    p = load_next_data_stream(file_ptr, data, data_len);
    index = (p * SAMPLESPERPAGE);
    read_page(data, index, calibration_offset, calibration_gain, num_pages,
              acc);
  }

  pages_per_thread = (num_pages - 1 - p) / NUMTHREADS;

#pragma omp parallel private(index, data) num_threads(NUMTHREADS)
  {
    for (int i = 0; i < pages_per_thread; i++) {
      if (is_corrupt == 0) {
#pragma omp critical
        {
          p = load_next_data_stream(file_ptr, data, data_len);
          index = (p * SAMPLESPERPAGE);
        }

        if (is_corrupt == 0) {
          read_page(data, index, calibration_offset, calibration_gain,
                    num_pages, acc);
        }
      }
    }
  }

  return is_corrupt;
}

void mexFunction(int nlhs, mxArray *plhs[], int nrhs, const mxArray *prhs[]) {
  int device_id;
  int samp_freq;
  char start_time[23];
  double calibration_gain[3];
  double calibration_offset[3];
  int expected_num_pages;
  double *acc;
  char *file_path;
  FILE *file_ptr;

  if (nrhs != 1) {
    mexErrMsgIdAndTxt("Pipeline:Read:WrongInput", "One input required.");
  }

  if (nlhs > 4) {
    mexErrMsgIdAndTxt("Pipeline:Read:WrongOutputs", "Too many outputs.");
  }

  if (mxIsChar(prhs[0]) != 1) {
    mexErrMsgIdAndTxt("Pipeline:Read:WrongInput", "Input must be a string.");
  }

  file_path = mxArrayToString(prhs[0]);
  if (!(file_ptr = fopen(file_path, "r"))) {
    mexErrMsgIdAndTxt("Pipeline:Read:BadFilePath",
                      "File does not exist or is corrupt.");
  }
  mxFree(file_path);

  if (!(read_header(file_ptr, &device_id, &samp_freq, start_time,
                    calibration_gain, calibration_offset,
                    &expected_num_pages) == 0)) {
    mexErrMsgIdAndTxt("Pipeline:Read:CorruptFile", "File could not be read.");
  }

  plhs[0] = mxCreateDoubleMatrix((mwSize)(expected_num_pages * SAMPLESPERPAGE),
                                 (mwSize)3, mxREAL);
  acc = mxGetPr(plhs[0]);
  if (!(read_bin_data(file_ptr, calibration_gain, calibration_offset,
                      expected_num_pages, acc) == 0)) {
    fclose(file_ptr);
    page = 0;
    is_corrupt = 0;
    mexErrMsgIdAndTxt("Pipeline:Read:PageCheckFailed",
                      "Less pages than expected.");
  }

  fclose(file_ptr);

  if (!(expected_num_pages == (page + 1))) {
    mexErrMsgIdAndTxt(
        "Pipeline:Read:PageCheckFailed",
        "Actual number of pages did not match expected number of pages.");
  }

  plhs[1] = mxCreateDoubleScalar(samp_freq);
  plhs[2] = mxCreateString(start_time);
  plhs[3] = mxCreateDoubleScalar(device_id);
  page = 0;

  return;
}
