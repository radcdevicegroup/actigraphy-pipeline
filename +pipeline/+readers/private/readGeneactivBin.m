function [acc, fs, startTime, id] = readGeneactivBin(filePath)
%READGENEACTIVBIN parse a GENEActiv generated bin file
%   [ACC, FS, STARTIME, ID] = READGENEACTIVBIN(FILEPATH) From a GENEActiv
%   generated bin grab the accelerometer data as ACC, the sampling frequency
%   used to record the accelerometer signal as FS, the start of the recording
%   as STARTIME, and the device ID as ID.
%
%   This function is intended to be a helper function for READGENEACTIV which
%   calculates useful information based on the results of this function. Use
%   READGENEACTIV instead.
%
%   Important efficiency notice:
%       This function is written in both C and MATLAB (as a fallback when the
%       C file has not been compiled). The C version is much more efficient
%       (~1/6 the time for larger files). To take advantage of the C file
%       compile it using mex with the following command:
%         > mex -R2018a CFLAGS="\$CFLAGS -fopenmp" LDFLAGS="\$LDFLAGS
%         -fopenmp" -lm readGeneactivBin.c
%
%       Run on the MATLAB command-line or on a shell command line. This command
%       requires openmp be installed.
%
%  See also READGENEACTIV.

    warning(['Using MATLAB version of READGENEACTIVBIN because C version' ...
             'has not been compiled. See help readGeneactivBin for more info.'])
	fileStr = fileread(filePath);
	[startTime, fs, dataStartIndex, calibrationData, id] = ...
		readHeader(fileStr);

	[acc, numSamples] = ...
		readAndConvertBinData(fileStr, dataStartIndex);
	acc = calibrateAcc(acc, calibrationData);

	function [startTime, sampFreq, dataStartIndex, calibrationData, id] = ...
            readHeader(fileStr)

		startTime = getStartTime(fileStr);
		sampFreq = getSamplingFreq(fileStr);
		dataStartIndex = regexp(fileStr, 'Recorded Data', 'once');
		header = fileStr(1:(dataStartIndex - 3));
		calibrationData = getCalibrationData(fileStr);
		id = getDeviceInfo(header);

		function startTime = getStartTime(fileStr)
			timeIndex = regexp(fileStr, 'Page Time', 'once');
			startTime = fileStr(timeIndex + 10:timeIndex + 32);
		end

		function sampFreq = getSamplingFreq(fileStr)
			sampFreq = regexp(fileStr,  'Measurement Frequency:(.*) Hz', ...
							  'tokens', 'once');
			sampFreq = str2double(sampFreq{1});
		end

		function calibrationData = getCalibrationData(fileStr)
			searchPattern = 'Calibration Data[A-Za-z0-9\n\r:-\s]*Volts';
			calibrationData = regexp(fileStr, searchPattern, 'match', 'once');
			calibrationData = regexp(calibrationData, '[0-9-]*', 'match');
		end

		function id = getDeviceInfo(header)
			id = regexp(header, 'Device Unique Serial Code:(\d*)', 'tokens', 'once');
			id = id{1};
		end
	end
end

function [acc, numSamples] = readAndConvertBinData(fileStr, dataStartIndex)
	findPatternIndex = @(pattern) regexp(fileStr, pattern, 'once');
	charsPerPageInitial = findPatternIndex('Sequence Number:1') - ...
		findPatternIndex('Sequence Number:0');

	pagesStr = regexp(fileStr(dataStartIndex:-1:1), '[0-9]+', 'once', 'match');
	numPages = str2double(pagesStr(end:-1:1));

	if ~iscompletebin(numPages, fileStr)
		error('MATLAB:corruptFile', 'Number of pages not equal to check.')
	end

	pagesOfDataPerLoop = 10;
	samplesPerPage = 300;
	hexCharsPerPage = samplesPerPage * 12;
	numSamples = samplesPerPage * numPages;

	startIdx = 1;
	hexStart = dataStartIndex + charsPerPageInitial - 2 - hexCharsPerPage;

	acc = zeros(numPages * 300, 3);
	indeces = getIndecesOfAccelDataInHex(pagesOfDataPerLoop);
	hexChars = zeros(hexCharsPerPage * pagesOfDataPerLoop, 1);

	for pageNum = 1:pagesOfDataPerLoop:numPages
		if pageNum + pagesOfDataPerLoop - 1 > numPages
			pagesOfDataPerLoop = numPages - pageNum;
			indeces = getIndecesOfAccelDataInHex(pagesOfDataPerLoop);
		end

		for pageStepper = 0:(pagesOfDataPerLoop - 1)
			exactPageNum = pageNum + pageStepper;
			charsPerPage = charsPerPageInitial + floor(log10(exactPageNum));

			hexStoreStart = pageStepper * hexCharsPerPage + 1;
			hexChars(hexStoreStart:(hexStoreStart + hexCharsPerPage - 1)) = ...
				fileStr(hexStart:(hexStart + hexCharsPerPage - 1));

			hexStart = hexStart + charsPerPage;
		end

		acc(startIdx:(startIdx + 300 * pagesOfDataPerLoop - 1), :) = ...
			hex2acc(hexChars, indeces);

		startIdx = startIdx + 300 * pagesOfDataPerLoop;
	end
	acc = convertMsbToSignBit(acc);

	function flag = iscompletebin(numPages, fileStr)
		pattern = strcat('\d*', reverse('Sequence Number:'));
		lastPageNum = reverse(regexp(reverse(fileStr), pattern, 'once', 'match'));
		lastPageNum = str2double(regexp(lastPageNum, '\d*', 'once', 'match')) + 1;
		flag = numPages == lastPageNum;
	end

	function indeces = getIndecesOfAccelDataInHex(pagesOfDataPerLoop)
		hexCharsPerDatum = 3;
		shapeIndeces = [hexCharsPerDatum, ...
						hexCharsPerPage * pagesOfDataPerLoop / hexCharsPerDatum];

		indeces = reshape(1:(hexCharsPerPage * pagesOfDataPerLoop), shapeIndeces)';

		numSignals = 4;
		numRows = samplesPerPage * numSignals;

		desiredRows = mod(1:(numRows * pagesOfDataPerLoop), 4) ~= 0;
		desiredRows = desiredRows' .* (1:(numRows * pagesOfDataPerLoop))';
		indeces = indeces(desiredRows ~= 0, :);
	end

	function acc = hex2acc(hexString, indeces)
		acc = hex2dec(hexString(indeces));
		acc = reshape(acc, [3 length(acc) / 3])';
	end

	function acc = convertMsbToSignBit(acc)
		N = 12;
		acc = 2 ^ (N - 1) - acc;
		acc = sign(acc) .* (2 ^ (N - 1) - abs(acc));
	end
end

function acc = calibrateAcc(acc, calibrationData)
	[x_gain, x_offset, y_gain, y_offset, z_gain, z_offset] = calibrationData{:};

	acc = acc * 100 - [str2double(x_offset), str2double(y_offset), str2double(z_offset)];
	acc = acc ./ [str2double(x_gain), str2double(y_gain), str2double(z_gain)];
end
