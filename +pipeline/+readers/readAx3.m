function [acc, fs, startTime, endTime] = readAx3(file)
    info = AX3_readFile(file, 'info', 1);
	tmp = AX3_readFile(file, ...
                       'UseC', 1, ...
                       'modality', [1 0 0], ...
                       'validPackets', info.validPackets);

    acc = tmp.ACC(:, 2:4);
    startTime = datetime(info.start.str);
    endTime = datetime(info.stop.str);
    fs = estimateFs;
    time = convertTo(datetime(tmp.ACC(:, 1), 'ConvertFrom', 'datenum'), ...
                     'posixtime')';
    acc = resample(acc, time, fs);

    function fs = estimateFs
        fs = round(length(acc) / seconds(endTime - startTime));
        base = floor(fs / 10) * 10;
        if fs > (base + 7.5)
            fs = base + 10;
        elseif fs < (base + 2.5)
            fs = base;
        else
            fs = base + 5;
        end
    end
end
