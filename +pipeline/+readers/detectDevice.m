function device = detectDevice(filename)
%DETECTDEVICE Determine the device used to create file
%   DEVICE = DETECTDEVICE(FILENAME) use FILENAME and possibly it's contents
%   to determine the type of DEVICE used to create the file.

    [~, ~, ext] = fileparts(filename);
    switch lower(ext)
      case {'.csv', '.txt', '.awc'}
        device = 'Actical';
      case '.bin'
        device = 'GENEActiv';
      case '.cwa'
        device = 'AX3';
      case '.edf'
        device = 'WatchPAT';
      case '.mat'
        device = 'Mat file';
      otherwise
        error('Pipeline:Reader:UnknownDevice', ...
              'Could not determine device of %s', filename);
    end
end
