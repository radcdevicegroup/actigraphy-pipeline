function [heartBeat, fs, startTime, endTime, deviceId] = readWatchpat(filepath)
	[header, signalHeader, signals] = blockEdfLoad(filepath);
	heartBeat = signals{1};
	fs = signalHeader(1).samples_in_record / header.data_record_duration;
	startTime = datetime([header.recording_startdate ' ' header.recording_starttime], ...
						 'InputFormat', 'dd.MM.yy HH.mm.ss');
	duration = header.num_data_records * header.data_record_duration;
	endTime = startTime + seconds(duration);
	deviceId = header.local_rec_id;
end
