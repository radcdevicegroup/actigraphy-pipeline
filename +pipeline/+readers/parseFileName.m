function [projId, fuYear, deviceId] = parseFileName(file, regexList)
%PARSEFILENAME use regex patterns to try to read a file name
%   [PROJID, FUYEAR, DEVICEID] = PARSEFILENAME(FILE, REGEXLIST) get meaningful
%   information out of a name FILE using one of the naming convention regex in
%   REGEXLIST. All outputs default to '' if none of the patterns match
%   or if the pattern doesn't specify a value for the specific value.

    projId = '';
    fuYear = '';
    deviceId = '';
    str1 = " ";

%     [~, name] = fileparts(file);
 [~, name, ext] = fileparts(file);
 
 if ext == '.txt'
     
    for pattern = regexList'
        n = regexp(name, pattern{1}, 'names', 'once');

        if ~isempty(n)
            fields = fieldnames(n);

            if ismember('projId', fields)
                projId = n.projId;
                projId = str1 + projId;
            end

            if ismember('fuYear', fields)
                fuYear = str2num(n.fuYear);
            end

            if ismember('deviceId', fields)
                deviceId = n.deviceId;
            end

            return
        end
    end
    
 elseif ext == '.bin'
     for pattern = regexList[2];
        n = regexp(name, pattern{2}, 'names', 'once');

        if ~isempty(n)
            fields = fieldnames(n);

            if ismember('projId', fields)
                projId = n.projId;
                projId = str1 + projId;
            end

            if ismember('fuYear', fields)
                fuYear = str2num(n.fuYear);
            end

            if ismember('deviceId', fields)
                deviceId = n.deviceId;
            end

            return
        end
    end
    
 elseif ext == '.cwa' | ext == '.mat'
     for pattern = {'^(?<deviceId>\d{5})_(?<projId>\d{8})_(?<fuYear>\d{2})_(?<placeDate>\d{8}$)'}
        n = regexp(name, pattern{1}, 'names', 'once');

        if ~isempty(n)
            fields = fieldnames(n);

            if ismember('projId', fields)
                projId = n.projId;
                projId = str1 + projId;
            end

            if ismember('fuYear', fields)
                fuYear = str2num(n.fuYear);
            end

            if ismember('deviceId', fields)
                deviceId = n.deviceId;
            end

            return
        end
     end
 else

    warning('Pipeline:Reader:FailedToParseName', ...
            'Did not recognize file name convention; all output values NA.');
end
