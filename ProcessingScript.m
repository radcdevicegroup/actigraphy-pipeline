
%Axivity
parent = ('C:\Users\mmakdah\Documents\AX3_DATA\to_be_processed');
files = dir(fullfile(parent, '**', '*.cwa'));

for i=1:length(files)
    
    parts = strsplit(files(i).folder, '\');
    month = parts{end};
    outputDir = fullfile('C:\Users\mmakdah\Documents\AX3_DATA\Processed\2021', month);
    if ~exist(outputDir, 'dir')
        mkdir(outputDir);
    end
    input = fullfile(parent, month, files(i).name);

    pipeline.run(input, outputDir, 'MINIMUMDAYS', 'None');
end




