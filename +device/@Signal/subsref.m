function varargout = subsref(obj, s)
%SUBSREF index a signal's data.
%   OBJ(S) return OBJ.DATA(S). When indexing via parenthesis the indices are
%   passed through to OBJ's DATA property. Otherwise, SUBSREF works as normal.
%
%   See also SUBSREF.

    switch s(1).type
      case '()'
        [varargout{1:nargout}] = builtin('subsref', obj.data, s);
      otherwise
        [varargout{1:nargout}] = builtin('subsref', obj, s);
    end
end
