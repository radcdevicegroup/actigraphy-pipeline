function objCopy = copy(obj)
%COPY make a deep copy of a Signal object
%   OBJCOPY = COPY(OBJ) return a copy of OBJ such that altering one (copy or
%   original) will not affect the other.
%
%   See also SIGNAL, RECORD/COPY.

    objCopy = device.Signal(obj.data, ...
                            obj.fs, ...
                            obj.nAxes);
end
