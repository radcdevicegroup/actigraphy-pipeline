function data = reshape(obj, columnLength)
%RESHAPE fold signal into days or hours
%   RESHAPE(OBJ) reshapes OBJ.DATA so each column is one day of recording. If
%   OBJ.NAXES > 1 axes are moved to the third dimension.
%
%   RESHAPE(OBJ, COLUMNLENGTH) reshape into columns of length COLUMNLENGTH.
%   COULUMNLENGTH must divide the original length of the signal so
%   COLUMNLENGTH x N == LENGTH(OBJ) for some N. COLUMNLENGTH can be a number
%   or a keyword ('days', 'hours', or 'original'). Where 'original' reshapes
%   OBJ to it's original shape (i.e. before any prior reshaping).
%
%   DATA = RESHAPE(OBJ, ...) return a reshaped copy of OBJ.DATA without
%   modifying OBJ.
%
%   See also SIGNAL, DIM, SLICE, RECORD/RESHAPETODAYS.

    if obj.isReshaped
        % Revert to original shape.
        if nargout == 1
            data = reshape(obj.data, [(size(obj, obj.dim('time')) * ...
                                       size(obj, obj.dim('days'))) obj.nAxes]);
        else
            obj.data = reshape(obj.data, [(size(obj, obj.dim('time')) * ...
                                           size(obj, obj.dim('days'))) obj.nAxes]);
        end
    elseif nargout == 1
        data = obj.data;
    end

    if nargin == 1
        columnLength = 'days';
    end

    if ischar(columnLength)
        switch columnLength
          case 'days'
            samplesPerColumn = 3600 * 24 * obj.fs;
          case 'hours'
            samplesPerColumn = 3600 * obj.fs;
          case 'original'
            return
          otherwise
            error('"%s" is not an accepted value for columnLength', columnLength)
        end
        assert(mod(length(obj), samplesPerColumn) == 0, ...
               'Signal is not an integer value of %s long; slice first.', columnLength)
    else
        samplesPerColumn = columnLength;
        assert(mod(length(obj), samplesPerColumn) == 0, ...
               'Desired column length does not divide original length of signal.')
    end

    numColumns = length(obj) / samplesPerColumn;

    if nargout == 1
        data = reshape(data, [samplesPerColumn numColumns obj.nAxes]);
    else
        obj.data = reshape(obj.data, [samplesPerColumn numColumns obj.nAxes]);
    end
end
