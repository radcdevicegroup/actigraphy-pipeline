function slice(obj, indices)
%SLICE cut values of signal outside of indices.
%   SLICE(OBJ, INDICES) convert OBJ.DATA to OBJ.DATA(INDICES, :) (or
%   OBJ.DATA(INDICES, :, :) if NDIMS(OBJ.DATA) == 3).
%
%   See also SIGNAL, DEVICE/SLICE.

    if ndims(obj.data) == 2
        obj.data = obj.data(indices, :);
    else
        obj.data = obj.data(indices, :, :);
    end
end
