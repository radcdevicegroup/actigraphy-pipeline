function TF = isReshaped(obj)
%ISRESHAPED determine if the signal has already been reshaped.
%   TF = ISRESHAPED(OBJ) returns true if the signal has been reshaped into
%   days; false otherwise.
%
%   See also RESHAPE

    TF = false;
    if (obj.nAxes == 1) && (size(obj, 2) > 1)
        TF = true;
    elseif (obj.nAxes > 1) && (ndims(obj.data) > 2)
        TF = true;
    end
end
