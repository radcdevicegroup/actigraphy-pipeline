function l = length(obj)
%LENGTH return the length of a signal's time series.
%   L = LENGTH(OBJ) returns the length of OBJ.DATA along the time dimension.
%   This differs form the normal LENGTH = MAX(SIZE(X)); although, in practice,
%   the time dimension will likely always be the longest resulting in the same
%   value for L.
%
%   See also SIGNAL, SIZE, LENGTH.

    l = size(obj.data, obj.dim('time'));
end
