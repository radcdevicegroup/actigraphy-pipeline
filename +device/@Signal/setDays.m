function setDays(obj, indices)
%SETDAYS remove unwanted days from a signal
%   SETDAYS(OBJ, INDICES) return a signal object with only the days in INDICES.
%   See also SLICE.

    assert(max(indices) <= obj.getNumDays, ...
           'Index have length equal to the number of days in the recording.')

    wasReshaped = obj.isReshaped;
    if ~obj.isReshaped;
        obj.reshape;
    end

    if ndims(obj.data) == 2
        obj.data = obj.data(:, indices);
    else
        obj.data = obj.data(:, indices, :);
    end

    if ~wasReshaped
        obj.reshape('original');
    end
end
