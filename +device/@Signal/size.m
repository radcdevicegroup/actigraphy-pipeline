function varargout = size(obj, dim)
%SIZE the size of signal's data property
%   SZ = SIZE(OBJ) return the size of OBJ's DATA property as a row vector.
%   [NCOL, NROW] = SIZE(OBJ) return number of columns and rows to separate variables.
%   [SZ1, SZ2, SZ3, ...] = SIZE(OBJ) return the size of each dimension of of
%   a many dimensional DATA property.
%
%   See also SIGNAL, LENGTH, SIZE.

    if nargin == 1
        [varargout{1:nargout}] = size(obj.data);
    else
        varargout{1} = size(obj.data, dim);
    end
end
