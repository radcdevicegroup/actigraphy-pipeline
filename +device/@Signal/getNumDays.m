function numDays = getNumDays(obj)
%GETNUMDAYS number of full days the signal was recorded over
%   NUMDAYS = GETNUMDAYS(OBJ) return the number of days OBJ was recorded over.
%   NUMDAYS will be an integer value indicating only the full days recorded
%   (i.e. if the signal was recorded over 3 days and 18hrs NUMDAYS would be 3).

    if ~isnan(obj.fullDays)
        numDays = obj.fullDays;
        return
    elseif obj.isReshaped
        numDays = size(obj, obj.dim('days'));
    else
        numSeconds = length(obj) / obj.fs;
        numDays = floor(numSeconds / (24 * 3600));
    end

    obj.fullDays = numDays;
end
