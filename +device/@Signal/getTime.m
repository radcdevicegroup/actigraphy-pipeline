function time = getTime(obj)
%GETTIME generate a signal's time vector
%   TIME = GETTIME(OBJ) calculate signal object OBJ's time vector using the
%   signal's length and the sampling frequency. Time is relative starting at
%   0 seconds. Knowing the signal's start time, absolute time is start time
%   + TIME. SIZE(TIME) == SIZE(OBJ).
%
%   See also SIGNAL.

    signalLength = obj.length;
    if obj.isReshaped
        signalLength = signalLength * obj.getNumDays;
    end

    time = linspace(0, (signalLength - 1) / obj.fs, signalLength)';

    if obj.isReshaped && (obj.getNumDays > 1)
        time = reshape(time, [obj.length obj.getNumDays]);
    end
end
