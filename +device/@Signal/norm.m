function data = norm(obj)
%NORM calculate the euclidean norm across axes.
%   NORM(OBJ) replace OBJ.DATA with the euclidean norm across it's axes. If
%   OBJ only has a single axis OBJ is returned unmodified.
%
%   DATA = NORM(OBJ) if NARGOUT == 1, NORM is non-destructive. OBJ.DATA is
%   copied leaving OBJ unmodified.

    if obj.nAxes == 1
        return
    end

    privateNorm = @(x) sum(x .^ 2, obj.dim('axes')) .^ (0.5);
    if nargout == 0
        obj.data = privateNorm(obj.data);
        obj.nAxes = 1;
    else
        data = obj.data;
        data = privateNorm(data);
    end

end
