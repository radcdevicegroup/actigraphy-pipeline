function pos = dim(obj, name)
%DIM the position of a named dimension
%   AXES(OBJ, NAME) return the dimension number of NAME. NAME can be 'time',
%   'axes', or 'days' (if OBJ has been reshaped into days). If OBJ has not been
%   reshaped a value of 'days' for name will result in an error
%
%   See also RESHAPE.

    switch lower(name)
      case 'time'
        pos = 1;
      case 'axes'
        if obj.isReshaped
            pos = 3;
        else
            pos = 2;
        end
      case 'days'
        assert(obj.isReshaped, 'Signal does not have a days dimension.')
        pos = 2;
      otherwise
        error('Not a known name.')
    end
end
