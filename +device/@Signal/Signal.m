classdef Signal < handle
%SIGNAL data structure for storing signals.
%   SIG = SIGNAL(DATA, FS) return a signal with time series M x N size DATA.
%   Where M is the number of time points and N the number of axes. FS stores the
%   sampling frequency.
%
%   SIG = SIGNAL(DATA, FS, NAXES) explicitly give the number of axes the
%   sensor recorded. NAXES is used to determine if the data has been folded.
%   Should only be supplied when making copies of a SIGNAL.
%
%   Generally not used directly but as part of a DEVICE/RECORD object which
%   will handle all modifications of a SIGNAL object.
%
%   Properties:
%       data  - Time series data with nAxes column vectors.
%       fs    - Sampling frequency.
%       nAxes - Number of axes.
%
%   Methods:
%       size       - size(data)
%       length     - length of time series.
%       subsref    - Index data
%       slice      - data(indices, :)
%       getTime    - Calculate a time vector from fs starting at 0 seconds.
%       getNumDays - Calculate number of full days.
%       reshape    - Reshape the signal's time series into a matrix.
%       isReshaped - Check if the signal has been reshaped.
%       dim        - Find the position of a named dimension.
%       norm       - Euclidean norm across axes.
%       copy       - Make a copy of the signal.
%
%   See also DEVICE/RECORD, MOCKSIGNAL.

    properties (SetAccess = private)
        data double {mustBeNumeric}
        fs (1, 1) {mustBeNumeric, mustBePositive} = 1
        nAxes (1, 1) {mustBeInteger, mustBePositive} = 1
    end

    properties (GetAccess = private, SetAccess = private)
        fullDays = nan;
    end

    methods
        function obj = Signal(data, fs, nAxes)
            obj.data = data;
            obj.fs = fs;

            if nargin < 3
                obj.nAxes = size(data, 2);
            else
                obj.nAxes = nAxes;
            end
        end

        varargout = size(obj, dim)
        l = length(obj)
        b = subsref(obj, s);
        slice(obj, indices)
        time = getTime(obj)
        numDays = getNumDays(obj)
        TF = isReshaped(obj)
        data = reshape(obj, columnLength)
        pos = dim(obj, name)
        data = norm(obj)
        objCopy = copy(obj)
        setDays(obj, indices);
    end
end
