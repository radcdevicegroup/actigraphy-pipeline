function record = mockRecord(M, nSignals, fs)
%MOCKRECORDING generate a fake RECORD object for testing purposes.
%   RECORD = MOCKRECORD(M) return record with 1 signal (signal1) of size 1 x M.
%   RECORD.ENDTIME is set to now and STARTTIME is calculated from ENDTIME, M, and
%   FS, which defaults to 1 / 100 Hz (i.e. one day is 864 samples).
%
%   RECORD = MOCKRECORD([M N]) Give signal N axes instead of 1.
%
%   RECORD = MOCKRECORD(..., NSIGNALS) return record with NSIGNALS number of
%   signals each of size given by M or [M N]. Signals are named in sequence
%   signal1, signal2, ....
%
%   RECORD = MOCKRECORD(..., NSIGNALS, FS) set each signal's sampling frequency to fs.
%
%   See also RECORD, MOCKSIGNAL.

    if numel(M) == 1
        N = 1;
    else
        N = M(2);
        M = M(1);
    end

    if nargin == 1 || isempty(nSignals)
        nSignals = 1;
    end

    if nargin < 3
        fs = 0.01;
    end
    validateattributes([M N], {'numeric'}, {'integer', 'positive'});
    validateattributes(nSignals, {'numeric'}, {'integer', 'positive'});
    validateattributes(fs, {'numeric'}, {'positive'});

    deviceType = 'Mock';
    info.endTime = datetime();
    info.startTime = datetime - ((M / fs) / (3600 * 24));
    info.placement = 'NA';
    keys.deviceId = '0000';
    keys.projId = '0000';
    keys.fuYear = '00';
    for i = 1:nSignals
        name = sprintf('signal%i', i);
        signals.(name) = device.mockSignal(M, N, fs);
    end

    record = device.Record(keys, info, deviceType, signals);
end
