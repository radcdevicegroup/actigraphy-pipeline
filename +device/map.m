function map(func, list, varargin)
%MAP perform a function over all records in a list
%   MAP(FUNC, LIST) run FUNC on all record files in LIST. The expected contents
%   of LIST are dependent on FUNC, the function should do something meaningful
%   to the files. The function FUNC must except exactly one argument--a record
%   file. Useful when FUNC produces a side affect (like writing to a file).
%
%   MAP(FUNC, DIR) if DIR is the path to a directory, FUNC will be run on all
%   files in the directory. Also works for a list of directories, globs, and
%   a mix of files and directories (anything UTILS.LISTFILES can handle).
%
%   MAP(..., 'LOG', FILE) by default when FUNC fails a warning is printed to the
%   command line. If optional parameter LOG is a file path errors are recorded to
%   that file instead.
%
%   MAP(..., 'DEBUG', TRUE) if DEBUG set to TRUE then errors are thrown as
%   normal instead of writing to a log or recasting as a warning. This
%   provides more information about the error itself making it easier to
%   debug than when a warning is thrown.
%
%   See also DEVICE/RECORD, UTILS/LISTFILES.

    islogicalchar = @(x) (islogical(x) && ~x) || ischar(x);
    p = inputParser;
    p.addParameter('log', false, islogicalchar);
    p.addParameter('debug', false, @islogical);
    p.parse(varargin{:});

    list = utils.listFiles(list);
    if p.Results.log
        fid = fopen(p.Results.log, 'a');
        fprintf(fid, '\n--------------------------------------\n');
        fprintf(fid, '%s\n', datetime);
    end

    numRecords = length(list);
    for i = 1:numRecords
        utils.progress(i - 1, numRecords);
        try
            func(list{i});
        catch ME
            if p.Results.debug
                fprintf('\n\n')
                rethrow(ME);
            end

            if ~p.Results.log
                fprintf('\n')
                warning('\nFailed to process %s\n\t%s:%s', list{i}, ...
                        ME.identifier, ME.message);
            else
                fprintf(fid, '%s: %s\n', list{i}, ME.identifier);
            end
        end
    end
    utils.progress(numRecords, numRecords);
    fprintf('\n');

    if p.Results.log
        fclose(fid);
    end
end
