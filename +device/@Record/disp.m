function disp(obj)
    props = properties(obj);
    fieldWidth = size(char(props), 2);

    fprintf('  Record with properties:\n\n')
    for prop = props'
        switch prop{1}
          case {'keys', 'info'}
            fields = fieldnames(obj.(prop{1}));
            fprintf('\t%+*s:\n', fieldWidth, prop{1})
            fw2 = size(char(fields), 2);
            for field = fields'
                value = obj.(prop{1}).(field{1});
                if isnumeric(value)
                    value = num2str(value, '%d');
                end
                fprintf('\t%s%*s: ''%s''\n', repmat(' ', [1 fieldWidth + 2]), ...
                        fw2, field{1}, value)
            end
          case 'signals'
            fields = fieldnames(obj.(prop{1}));
            fw2 = size(char(fields), 2);
            fprintf('\t%+*s:\n', fieldWidth, prop{1})
            for signal = fieldnames(obj.(prop{1}))'
                fprintf('\t%s%*s (%ix%i)\n', repmat(' ', [1 fieldWidth + 2]), ...
                        fw2, signal{1}, size(obj.(prop{1}).(signal{1}).data))
            end
          case 'variables'
            fields = fieldnames(obj.(prop{1}));
            fprintf('\t%+*s: \n', fieldWidth, prop{1});
            fw2 = size(char(fields), 2);
            for field = fields'
                fprintf('\t%s%*s\n', repmat(' ', [1 fieldWidth + 2]), ...
                        fw2, field{1});
            end
          otherwise
            value = obj.(prop{1});
            if isnumeric(value)
                value = num2str(value, '%d');
            end
            fprintf('\t%+*s: ''%s''\n', fieldWidth, prop{1}, value)
        end
    end
    fprintf('\n')
end
