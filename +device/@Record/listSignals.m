function signalNames = listSignals(obj)
%LISTSIGNALS return all signals contained by a Record
%   SIGNALNAMES = LISTSIGNALS(OBJ) return a list of all signals Record OBJ holds.
%
%   See also RECORD, ADDSIGNAL, REMOVESIGNAL, SIGNAL/SIGNAL.

    signalNames = fieldnames(obj.signals);
end
