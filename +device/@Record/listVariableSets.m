function sets = listVariableSets(obj)
%LISTVARIABLESETS show all a record's variableSets
%   SETS = LISTVARIABLESETS(OBJ) return all VARIABLESETS associated with OBJ.
%   Variables calculated from the same signals are stored together in a set.
%   This assures variables calculated from devices with different signals can
%   still be printed to the same csv files because variable sets calculated
%   from the same signal will always be given the same name and always have
%   the same variables.
%
%   See also PRINT.

    sets = fieldnames(obj.variables);
end
