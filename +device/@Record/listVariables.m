function vars = listVariables(obj, variableSet)
%LISTVARIABLES return all variables for a given variable set
%   VARS = LISTVARIABLES(OBJ, VARIABLESET) provide all fields in the
%   OBJ.VARIABLES.VARIABLESET which corresponds to the names of all variables
%   stored for VARIABLESET.
%
%   See also PRINT.

    vars = fieldnames(obj.variables.(variableSet));
end
