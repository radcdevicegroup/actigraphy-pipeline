function slice(obj, t1, t2)
%SLICE cut all a Record's signals leaving only the data between T1--T2.
%   SLICE(OBJ, T1, T2) remove parts of all OBJ's signal's outside the time range
%   T1--T2. Times (T1 and T2) may be in posix form or a datetime. Times must be
%   between OBJ.INFO.STARTTIME--OBJ.INFO.ENDTIME.
%
%   See also SIGNAL/SLICE, DATETIME.

    if isa(t1, 'datetime')
        t1 = convertTo(t1, 'posix');
    end

    if isa(t2, 'datetime')
        t2 = convertTo(t2, 'posix');
    end

    startTime = convertTo(obj.info.startTime, 'posix');
    endTime = convertTo(obj.info.endTime, 'posix');

    if t1 > t2
        tmp = t1;
        t1 = t2;
        t2 = t1;
    end

    assert((t2 <= endTime) && (t1 >= startTime), ...
           'Times must be between startTime and endTime.')

    obj.mapSignals(@sliceSignal);

    function sliceSignal(sig)
        time = sig.getTime() + startTime;
        indeces = time >= t1 & time < t2;
        sig.slice(indeces);
    end

    obj.info.startTime = datetime(t1, 'convertfrom', 'posix');
    obj.info.endTime = datetime(t2, 'convertfrom', 'posix');
end
