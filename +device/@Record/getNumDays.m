function numDays = getNumDays(obj)
%GETNUMDAYS duration of a recording in days
%   NUMDAYS = GETNUMDAYS(OBJ) determine how many full days the recording took
%   place over. For trimmed records NUMDAYS will always be an integer value.
%   Untrimmed records may have fractions of a day.
%
%   See also TRIM, SIGNAL/GETNUMDAYS

    numDays = obj.info.numDays;
end
