function removeVariableSet(obj, varargin)
%REMOVEVARIBLESET drop a variableSet from a Record.
%   REMOVEVARIABLESET(OBJ, VARIABLESET) Removes the variableSet VARIABLESET from
%   Record OBJ's variable structure.
%
%   REMOVEVARIABLESET(OBJ, VARIABLESETLIST) Remove all sets in VARIABLESETLIST from
%   Record OBJ.
%
%   REMOVEVARIABLESET(OBJ, SIGNALA, SIGNALB, ...) Remove each set from OBJ.
%
%   See also RECORD, ADDVARIABLESET, LISTVARIABLESETS, SIGNAL/SIGNAL.

    if ~isempty(varargin)
        if length(varargin) > 1
            obj.removeVariableSet(varargin(2:end));
        end

        setName = varargin{1};
        if iscell(setName)
            if length(setName) > 1
                obj.removeVariableSet(setName(2:end));
            end

            obj.removeVariableSet(setName{1});
        else
            if ~contains(setName, obj.listVariableSets)
                warning('Signal "%s" does not exist; skipping.', setName)
            else
                obj.variables = rmfield(obj.variables, setName);
            end
        end
    end
end
