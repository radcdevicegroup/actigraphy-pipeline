function print(obj, fid, variableSet, varargin)
%PRINT write variableSets to summary files.
%   PRINT(OBJ, FID, VARIABLESET) Print the variables in VARIABLESET to a the
%   file identified by FID. VARIABLESET must be the name of a variable set
%   stored in OBJ (i.e. in OBJ.LISTVARIABLESETS). With the exception of two
%   special variable set names: 'records' and 'devices'. The records variable
%   set prints the info structure of the record and devices stores a list of
%   device id and device types.
%
%   All files are printed as csvs by default but the delimiter can be changed
%   (see below). Except for with the special 'devices' variable set, all
%   files are started with the record's key values (the values in OBJ.KEYS)
%   to create ensure all records can be uniquely identified across files.
%
%   If VARIABLESET has variables with vector values, N rows are printed in
%   the file for that record (for N =
%   LENGTH(OBJ.LISTVARIABLES(VARIABLESET))). Any scalar values (including the
%   record's keys) are repeated for each row. When there are multiple vector
%   values the sizes of all vectors should be the same length. Vector
%   variables are used when calculating values over multiple time points. For
%   example when a variable is calculated for each day.
%
%   PRINT(..., 'HEADER', TRUE) Print a header to csv before printing the data.
%   For creating new files.
%
%   PRINT(..., 'DELIM', DELIM) if a delimiter is supplied use this instead of
%   the default ',' to separate values. Should be the same for all rows and the
%   header.
%
%   Examples:
%       Example 1. printing a single file to a new csv.
%
%           % Some function that returns a record.
%           record = getRecord(1);
%
%           % Some variable calculator.
%           calculateVariables(record);
%
%           varSet = record.listVariableSets;
%           fid = fopen([varSet{1} '.csv'], 'w');
%          
%           % Print both header and data to the new file.
%           record.print(fid, varSet{1}, 'header', true);
%
%           fclose(fid);
%
%
%       Example 2. printing multiple records to a file.
%
%           fid = fopen('example.csv', 'w');
%           for i = 1:100;
%               record = getRecord(i);
%               calculateVariables(record);
%               if i == 1
%                   record.print(fid, 'example', 'header', true);
%               else
%                   % Only print data since header already exists.
%                   record.print(fid, 'example');
%               end
%           end
%           fclose(fid);
%
%   See also LISTVARIABLESETS and ADDVARIABLES.

    p = inputParser;
    p.addParameter('delim', ',', @ischar);
    p.addParameter('header', false, @islogical);
    p.parse(varargin{:});

    switch variableSet
      case 'records'
        structs = {obj.keys, obj.info};
      case 'devices'
        structs = {struct('deviceId', obj.keys.deviceId, 'deviceType', obj.deviceType)};
      otherwise
        structs = {obj.keys, obj.variables.(variableSet)};
    end

    if p.Results.header
        printHeader(structs{:});
    end

    printData(structs{:});

    function printHeader(varargin)
        names = cellfun(@(x) fieldnames(x), varargin, 'UniformOutput', false);
        names = makeSnakeCase(vertcat(names{:}));
        cellfun(@(x) fprintf(fid, '"%s"%s', x, p.Results.delim), names(1:end-1));
        fprintf(fid, '"%s"\n', names{end});
    end

    function printData(varargin)
        numStructs = length(varargin);
        numRows = cellfun(@(x) structfun(@countel, x), varargin, 'UniformOutput', false);
        numRows = vertcat(numRows{:});
        ismultirow = numRows > 1;

        if isempty(ismultirow) > 1
            assert(sum(numRows == max(numRows)) == sum(ismultirow), ...
                   'Record:Print:NonuniformOutput', ...
                   'All variables must have the same number of elements.')
        end
        numRows = max(numRows);

        for rowi = 1:numRows
            pos = 1;
            for structi = 1:numStructs
                fields = fieldnames(varargin{structi});
                numFields = length(fields);
                for fieldi = 1:numFields
                    value = varargin{structi}.(fields{fieldi});
                    if ismultirow(pos)
                        value = value(rowi);
                    end

                    if isa(value, 'datetime')
                        fprintf(fid, '%s', char(value, 'dd-MMM-uuuu hh:mm:ss'));
                    elseif isnumeric(value) || islogical(value)
                        fprintf(fid, '%g', value);
                    elseif ischar(value) && length(value) == 8
                        valuenew = convertCharsToStrings(value);
                        s1 = "'";
                        value = strcat(s1,valuenew);
                        fprintf(fid, '%s', value);
                    else
                        fprintf(fid, '%s', value);
                    end

                    if (fieldi == numFields) && (structi == numStructs)
                        fprintf(fid, '\n');
                    else
                        fprintf(fid, '%s', p.Results.delim);
                    end
                    pos = pos + 1;
                end
            end
        end
    end

    function n = countel(x)
        if isstr(x)
            n = 1;
        else
            n = numel(x);
        end
    end
end

function name = makeSnakeCase(name)
    name = regexprep(name, 'Id', 'id');
    name = lower(regexprep(name, '([A-Z])', '_$1'));
end

function str = sprintstruct(fmt, struct)
    str = '';
    for f = fieldnames(struct)'
        str = [str sprintf(fmt, struct(f{1}))];
    end
end
