function objCopy = copy(obj)
%COPY make a deep copy of a Record object
%   OBJCOPY = COPY(OBJ) return a copy of OBJ such that altering one (copy or
%   original) will not affect the other.
%
%   See also RECORD, SIGNAL/COPY.

    signalsCopy = obj.mapSignals(@copy);
    objCopy = device.Record(obj.keys, obj.info, obj.deviceType, signalsCopy, ...
                            'Variables', obj.variables, 'startOfDay', obj.startOfDay);
end
