function addSignal(obj, signalName, data, fs, nAxes)
%ADDSIGNAL associate a new signal with a Record
%   ADDSIGNAL(OBJ, SIGNALNAME, SIGNAL) add SIGNAL to Record OBJ's signals
%   structure under field name SIGNALNAME. If there is already a signal
%   referenced as SIGNALNAME in the Record it will be overwritten by the new
%   signal.
%
%   ADDSIGNAL(OBJ, SIGNALNAME, DATA, FS, NAXES) create a new signal from
%   DATA, FS, and NAXES and add it to Record OBJ.
%
%   See also RECORD, SIGNAL/SIGNAL, REMOVESIGNAL, LISTSIGNALS.

    if obj.hasSignal(signalName)
        warning('Signal "%s" already exists; overwriting.', signalName)
    end

    if isa(data, 'device.Signal')
        assert(nargin == 3, 'Wrong number of arguments.')
        signal = data;
    else
        signal = device.Signal(data, fs, nAxes);
    end
    obj.signals.(signalName) = signal;
end
