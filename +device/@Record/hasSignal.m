function TF = hasSignal(obj, signalName)
%HASSIGNAL test if a Record has a specific signal
%   TF = HASSIGNAL(OBJ, SIGNALNAME) return true if the Record OBJ contains
%   signal SIGNALNAME.
%
%   See also RECORD, SIGNAL/SIGNAL, ADDSIGNAL, REMOVESIGNAL.

    TF = ismember(signalName, fieldnames(obj.signals));
end
