classdef Record < handle
%RECORD a class for managing records and their sensor data
%   RECORD = RECORD(KEYS, INFO, DEVICETYPE, SIGNALS) construct a new record.
%   KEYS is structure of identifying data consisting of fields 'projId',
%   'fuYear', and 'deviceId' all of which should be doubles. At the time of
%   construction, the structure INFO must contain the 'startTime', 'endTime',
%   and 'placement', the first two should be DATETIMES and the last should be a
%   string. DEVICETYPE is a string naming the device used to create the
%   record. SIGNALS is a structure of all the signals recorded by the device.
%   All SIGNALS should be of type DEVICE.SIGNAL.
%
%   RECORD = RECORD(..., 'STARTOFDAY', STARTOFDAY) specify a start of day for
%   trimming the record in 24h time. On construction RECORD is trimmed to remove
%   data before STARTOFDAY on the first day and data after STARTOFDAY on the
%   last day. For records that should not be trimmed set STARTOFDAY to 'NONE'.
%   Default value is 19.
%
%   RECORD = RECORD(..., 'QCFUNC', QCFUNC) use the function QCFUNC to remove
%   days from the record. QCFUNC should accept a record object and return a
%   vector of days to keep (i.e. true indices are kept false are removed).
%   The size of the days worn vector must be equal to the number of days in
%   the record. RECORD.RESHAPETODAYS is useful for designing QCFUNCs. Defaults
%   to 'NONE'.
%
%   RECORD = RECORD(..., 'VARIABLES', VARIABLES) if a VARIABLES structure is
%   passed to the constructor this will be used instead of creating a new
%   empty structure. This allows pre-populating RECORD with variables. This
%   was specifically added for RECORD.COPY and should be used sparingly.
%   Whenever possible calculating variables after RECORD has been constructed
%   should be preferred.
%
%   In general RECORDs construction should be left to a reader for ease of
%   use rather than constructed manually.
%
%   Properties:
%       keys       - A structure of data that uniquely identifies the record.
%       info       - Additional information about the recording.
%       deviceType - The name of the type of device used to create the recording.
%       signals    - A structure containing each of the recorded signals.
%       variables  - A structure of all the variables calculated for each signal.
%       startOfDay - The start of the day used to trim the data.
%
%   Methods:
%       save             - Save the record to filename.
%       copy             - Make a deep copy of the record.
%       hasSignal        - Test if the record contains a specific signal.
%       addSignal        - Add associate a new signal to the record.
%       removeSignal     - Remove an associated signal by name.
%       listSignals      - List all signals stored in the record.
%       getSignal        - Return a signal's handle or property.
%       mapSignal        - Call a function on all signals in a record.
%       slice            - Remove all record data outside of a time range.
%       trim             - Cut signals before first day and after last day.
%       reshapeToDays    - Fold signals into a matrix of day long columns.
%       getNumDays       - Return the duration of the record in days.
%       setDaysWorn      - Remove days when the device was not worn.
%       addVariable      - Add variable for the associated signal.
%       removeVariable   - Remove a variable.
%       listVariables    - List all variables for a variable set.
%       listVariableSets - List all a records variable sets.
%       print            - Print variable sets to a csv.
%
%   Warning:
%       This class is a subclass of the handle class. Objects of this class are
%       not copied liked most data types in MATLAB. Instead they are mutable and
%       many of the class's methods will mutate the data. When this is not
%       desired explicitly make a copy of the object ahead of any destructive
%       actions.
%
%   See also SIGNAL, MOCKRECORD, HANDLE.

    properties (SetAccess = private)
        keys struct
        info struct
        deviceType char
        signals struct
        variables struct
        startOfDay
    end

    methods
        function obj = Record(keys, info, deviceType, signals, varargin)
            requiredKeys = {'projId', 'fuYear', 'deviceId'};
            requiredInfo = {'startTime', 'endTime', 'placement'};
            fullInfo = {requiredInfo{:}, 'numDays', 'maxConsecutiveDays'};

            assertIsComplete(keys, requiredKeys, 'keys');
            assumeCopy = false;
            if isequal(sort(fullInfo), sort(fieldnames(info)'))
                assumeCopy = true;
            else
                assertIsComplete(info, requiredInfo, 'info');
            end

            obj.keys = keys;
            obj.info = info;
            obj.deviceType = deviceType;

            if isempty(signals)
                warning('Record initialized without any signals.')
            else
                assert(arefields(signals, 'device.Signal'), ...
                       'All signals must be of type device.Signal.')
            end
            obj.signals = signals;
                        
            isfunc = @(h) (isa(h, 'function_handle')) || (strcmpi(h, 'None'));
            ishour = @(h) (utils.isnaturalnumber(h)) || (strcmpi(h, 'None'));

            p = inputParser;
            p.addParameter('variables', struct(), @isstruct);
            p.addParameter('qcFunc', 'None', isfunc);
            p.addParameter('startOfDay', 19, ishour);
            p.parse(varargin{:});

            obj.variables = p.Results.variables;
            obj.startOfDay = p.Results.startOfDay;

            if assumeCopy
                return
            end

            if ~strcmpi(obj.startOfDay, 'None')
                obj.trim;
            end

            if ~strcmpi(p.Results.qcFunc, 'None')
                dayIndeces = p.Results.qcFunc(obj);
                obj.info.numDays = floor(days(obj.info.endTime - ...
                                               obj.info.startTime));
                obj.setDaysWorn(dayIndeces);
            else
                dayIndeces = true(1, floor(days(obj.info.endTime - ...
                                             obj.info.startTime)));
            end

            obj.info.numDays = days(obj.info.endTime - obj.info.startTime);
            if numel(dayIndeces) > 1
                obj.info.maxConsecutiveDays = findMaxObjectLength(dayIndeces);
            else
                obj.info.maxConsecutiveDays = 0;
            end

            function assertIsComplete(structure, requiredFields, name)
                if ~isequal(sort(requiredFields), sort(fieldnames(structure)'))
                    fields = join(requiredFields, ', ');
                    error(['Record:' upper(name(1)) name(2:end)], ...
                          sprintf('Record must contain %s fields: %s.', ...
                                  name, fields{1}));
                end
            end

            function TF = arefields(item, type)
                assert(isa(item, 'struct'), 'Input must be a structure.')

                TF = true;
                for field = fieldnames(item)'
                    if ~isa(item.(field{1}), type)
                        TF = false;
                        return
                    end
                end
            end

            function maxLength = findMaxObjectLength(objectList)
                labeledObjects = bwlabel(objectList);
                labeledObjects = labeledObjects == (1:length(labeledObjects))';
                maxLength = max(sum(labeledObjects, 2));
            end
        end

        disp(obj)
        save(obj, filename)
        objCopy = copy(obj)
        TF = hasSignal(obj, signalName)
        addSignal(obj, signalName, signal, fs, nAxes)
        removeSignal(obj, varargin)
        signalNames = listSignals(obj)
        item = getSignal(obj, signalName, propertyName)
        struct = mapSignals(obj, func)
        slice(obj, t1, t2)
        trim(obj, startOfDay)
        reshapeToDays(obj, startOfDay, varargin)
        numDays = getNumDays(obj)
        setDaysWorn(obj, daysWornIndex)
        addVariable(obj, variableSet, variable, value);
        removeVariable(obj, variableSet, variable);
        removeVariableSet(obj, variableSet);
        variableNames = listVariables(obj, variableSet);
        variableSets = listVariableSets(obj);
        print(obj, fid, variableSet, varargin)
    end
end
