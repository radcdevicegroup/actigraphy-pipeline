function trim(obj, startOfDay)
%TRIM remove data before the first whole day and after the last.
%   TRIM(OBJ) remove all data before OBJ.STARTOFDAY on the first day and
%   OBJ.STARTOFDAY on the last day. If OBJ.STARTOFDAY is 'NONE' returns an error.
%
%   TRIM(OBJ, STARTOFDAY) remove all data before STARTOFDAY on the first
%   day and after STARTOFDAY on the last day.
%
%   See also RECORD, SLICE, RESHAPETODAYS, SIGNAL/SLICE.

    if nargin == 1 || isempty(startOfDay)
        startOfDay = obj.startOfDay;
    end

    assert(~strcmpi(startOfDay, 'None'), 'Record:NoStartOfDay', ...
           ['Record''s start of day is set to "None". If record' ...
            'should be trimmed anyway provide startOfDay to trim.'])

    validateattributes(startOfDay, {'numeric'}, {'<', 24, '>=', 0})

    startThresh = obj.info.startTime;
    if cmpToHour(startThresh, @gt, startOfDay)
        startThresh.Day = startThresh.Day + 1;
    end
    startThresh.Hour = startOfDay;
    startThresh.Minute = 0;
    startThresh.Second = 0;

    endThresh = obj.info.endTime;
    if cmpToHour(endThresh, @lt, startOfDay)
        endThresh.Day = endThresh.Day - 1;
    end
    endThresh.Hour = startOfDay;
    endThresh.Minute = 0;
    endThresh.Second = 0;

    assert(days(obj.info.endTime - startThresh) >= 1, ...
           'Record:ShortRecording', ...
           'Recording is less than 24 hours from the desired start time.')

    obj.slice(startThresh, endThresh);

    function flag = cmpToHour(t, operator, hr)
        [h, m, s] = hms(t);
        t1 = datetime([00 00 00 h m s]);
        t2 = datetime([00 00 00 hr 00 00]);

        flag = operator(t1, t2);
    end
end
