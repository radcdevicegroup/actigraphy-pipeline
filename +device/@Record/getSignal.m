function item = getSignal(obj, signalName, propertyName)
%GETSIGNAL get a signal or property from a signal.
%   SIGNAL = GETSIGNAL(OBJ, SIGNALNAME) return the signal SIGNALNAME
%   (OBJ.SIGNALS.SIGNALNAME).
%
%   PROPERTY = GETSIGNAL(OBJ, SIGNALNAME, PROPERTYNAME) return SIGNALNAME's
%   property PROPERTYNAME (OBJ.SIGNALS.SIGNALNAME.PROPERTYNAME).
%
%   See also RECORD, SIGNAL.

    if nargin == 2
        item = obj.signals.(signalName);
    else
        item = obj.signals.(signalName).(propertyName);
    end
end
