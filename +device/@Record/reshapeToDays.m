function reshapeToDays(obj, startOfDay, varargin)
%RESHAPETODAYS reshape all a record's signals into days.
%   RESHAPETODAYS(OBJ) reshape all device's signals to an m x n matrix with
%   m = (sampleFreq * seconds per day) and n = (number of whole days). If a
%   signal has not been trimmed, to have a whole number of days, already the
%   record is trimmed first. If a signal has multiple axes those will be moved
%   to the third axis.
%
%   RESHAPETODAYS(OBJ, STARTOFDAY) uses startOfDay in 24hr format as the cutoff
%   for each day. Defaults to OBJ.STARTOFDAY.
%
%   RESHAPETODAYS(OBJ, STARTOFDAY, SIGNALS...) restrict reshaping to the listed
%   signals. If SIGNALS contain non-existent signals they are ignored. SIGNALS
%   can either be a cell of values or individual strings. Warning: all
%   signals in the Record will be stripped via TRIM to ensure startTime and
%   endTime apply to all signals.
%
%   Examples:
%
%       Example 1: Reshape all signals starting at record.startOfDay.
%           record.reshapeToDays();
%
%       Example 2: Reshape all signals starting at 3pm.
%           record.reshapeToDays(15);
%
%       Example 3: Reshape signals 'counts' and 'accelerometer' to days.
%           record.reshapeToDays([], 'counts', 'accelerometer');
%           % or
%           signals = {'counts', 'accelerometer'};
%           record.reshapeToDays([], signals);
%           % Because accelerometer signals have 3 axes
%           size(dev.signals.accelerometer)
%           % will be m x n x 3. While
%           size(dev.signals.counts)
%           % will be m x n.
%
%   See also RECORD, TRIM, SIGNAL, SIGNAL/RESHAPE.

    if nargin == 1 || isempty(startOfDay)
        startOfDay = obj.startOfDay;
    end

    if strcmpi(startOfDay, 'None')
        assert(~strcmpi(startOfDay, 'None'), 'Record:NoStartOfDay', ...
               ['Record''s start of day is set to "None". If record' ...
                'should be trimmed anyway provide startOfDay to trim.'])
    end

    validateattributes(startOfDay, {'numeric'}, {'<', 24, '>=', 0})
    signals = parseArguments();

    if isempty(signals)
        warning('No signals to reshape; returning early.')
        return
    end

    try
        obj.trim(startOfDay);
    catch ME
        if ME.identifier('Record:ShortRecording')
            warning('Recording shorter than a day. Returning original data.')
        else
            rethrow(ME)
        end
    end

    for signal = signals'
        obj.signals.(signal{1}).reshape;
    end

    function signals = parseArguments()
        if length(varargin) > 1
            signals = varargin;
        elseif ~isempty(varargin)
            signals = varargin{1};
            if ~iscell(signals)
                signals = {signals};
            end
        else
            signals = obj.listSignals;
        end

        if isrow(signals)
            signals = signals';
        end

        for signal = signals'
            if ~obj.hasSignal(signal{1})
                warning('Record does not have signal "%s"; ignoring.', signal{1})
                signals = signals(~strcmp(signals, signal{1}));
            end
        end

        if isempty(signals)
            return
        end

        validateattributes(signals, {'cell'}, {})
        validateattributes(signals{1}, {'char'}, {})
    end
end
