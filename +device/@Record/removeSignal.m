function removeSignal(obj, varargin)
%REMOVESIGNAL drop signals from a Record.
%   REMOVESIGNAL(OBJ, SIGNALNAME) Removes the signal SIGNALNAME from Record
%   OBJ's signal structure.
%
%   REMOVESIGNAL(OBJ, SIGNALLIST) Remove all signals in SIGNALLIST from
%   Record OBJ.
%
%   REMOVESIGNAL(OBJ, SIGNALA, SIGNALB, ...) Remove each signal from OBJ.
%
%   See also RECORD, ADDSIGNAL, LISTSIGNALS, SIGNAL/SIGNAL.

    if ~isempty(varargin)
        if length(varargin) > 1
            obj.removeSignal(varargin(2:end));
        end

        signalName = varargin{1};
        if iscell(signalName)
            if length(signalName) > 1
                obj.removeSignal(signalName(2:end));
            end

            obj.removeSignal(signalName{1});
        else
            if ~obj.hasSignal(signalName)
                warning('Signal "%s" does not exist; skipping.', signalName)
            else
                obj.signals = rmfield(obj.signals, signalName);
            end
        end
    end
end
