function save(obj, filename)
%SAVE a Record to a mat file
%   SAVE(OBJ, FILENAME) save the Record OBJ, to FILENAME. Use
%   RECORD.LOAD(FILENAME) to read in the device later.
%
%   See also SAVE, LOAD, RECORD.

    save(filename, 'obj');
end
