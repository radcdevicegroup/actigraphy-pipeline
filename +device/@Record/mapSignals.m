function out = mapSignals(obj, func)
%MAPSIGNALS call a function on all signals in a record
%   MAPSIGNALS(OBJ, FUNC) apply FUNC to all OBJ's signals.
%   STRUCT = MAPSIGNALS(OBJ, FUNC) returns a structure with a field named
%   after each signal holding the return value of FUNC for that signal.

    for signalName = obj.listSignals'
        switch nargout
          case 0
            func(obj.signals.(signalName{1}));
          case 1
            out.(signalName{1}) = func(obj.signals.(signalName{1}));
          otherwise
            error('Device:TooManyOutputs', ...
                  'Map signal returns at most one value.')
        end
    end
end
