function removeVariable(obj, variableSet, variable)
%REMOVEVARIABLE delete a variable from a variableSet
%   REMOVEVARIABLE(OBJ, VARIABLESET, VARIABLE) if VARIABLESET has a variable
%   named VARIABLE remove it from the structure.

    if nargin == 2
        obj.variables = rmfield(obj.variables, variableSet);
    else
        obj.variables.(variableSet) = rmfield(obj.variables.(variableSet), variable);
    end
end
