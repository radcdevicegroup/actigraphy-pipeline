function addVariable(obj, variableSet, variable, value)
%ADDVARIABLE add a new variable to a variableSet
%   ADDVARIABLE(OBJ, VARIABLESET, VARIABLE, VALUE) set VALUE to VARIABLE in
%   set VARIABLESET. If the variable already exists in VARIABLESET is it replaced.
%
%   See also PRINT.

    obj.variables.(variableSet).(variable) = value;
end
