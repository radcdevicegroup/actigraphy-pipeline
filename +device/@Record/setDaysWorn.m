function setDaysWorn(obj, daysWornIndex)
%SETDAYSWORN remove days from a recording
%   SETDAYSWORN(OBJ, DAYSWORNINDEX) remove days not in DAYSWORNINDEX.
%   DAYSWORNINDEX should be a logical vector of length OBJ.GETNUMDAYS. Days
%   corresponding to false indices are removed from the record.
%
%   Alters start and end time of record if these are affected by removing
%   days. It does not, however, change OBJ.INFO.NUMDAYS or
%   OBJ.INFO.MAXCONSECUTIVEDAYS. This may change later but this function
%   should probably not be used outside of record construction, which handles
%   changes to number of days and consecutive days outside itself.

    numDays = obj.getNumDays;
    assert(length(daysWornIndex) == numDays, ...
           'Index must have length equal to the number of days in the recording.')

    startTime = convertTo(obj.info.startTime, 'posix');
    days = find(daysWornIndex) - 1;
    obj.mapSignals(@sliceSignal);

    function sliceSignal(sig)
        secondsPerDay = 3600 * 24;
        time = sig.getTime() + startTime;
        indices = false(1, length(sig));
        for day = 1:length(days')
            dayStart = startTime + (day * secondsPerDay);
            dayEnd = dayStart + secondsPerDay;
            indices((time >= dayStart) & (time < dayEnd)) = true;
        end
        sig.slice(indices);
    end

    obj.info.startTime.Day = obj.info.startTime.Day + days(1);
    obj.info.endTime.Day = obj.info.endTime.Day - (numDays - (days(end) + 1));
end
