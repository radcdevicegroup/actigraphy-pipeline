function signal = mockSignal(M, nAxes, fs)
%MOCKSIGNAL generate a fake SIGNAL object for testing purposes.
%   SIGNAL = MOCKSIGNAL(M) return an M x 1 sized signal. Data is 1:M
%   and sample frequency is 1 / 100 Hz (i.e. one day is 864 samples).
%
%   SIGNAL = MOCKSIGNAL(M, N) return an M length signal with N axes. All axes
%   are 1:M (i.e. data = REPMAT((1:M)', [1 N]))).
%
%   SIGNAL = MOCKSIGNAL(M, N, FS) set sampling frequency to fs.
%
%   See also SIGNAL, MOCKRECORD.

    if nargin == 1 || isempty(nAxes)
        nAxes = 1;
    end

    if nargin < 3
        fs = 0.01;
    end
    validateattributes(M, {'numeric'}, {'integer', 'positive'});
    validateattributes(nAxes, {'numeric'}, {'integer', 'positive'});
    validateattributes(fs, {'numeric'}, {'positive'});

    column = (1:M)';
    data = repmat(column, [1 nAxes]);

    signal = device.Signal(data, fs);
end
