classdef getTimeTest < matlab.unittest.TestCase & matlab.perftest.TestCase

	properties
		len = 100
		signal1
		signal2
		signal3
		days4
		signal4
	end

	methods (TestMethodSetup)
		function setupMockSignal(testCase)
			nAxes = 3;
			fs = 0.01;
			testCase.signal1 = device.mockSignal(testCase.len, [], fs);
			testCase.signal2 = device.mockSignal(testCase.len, nAxes, fs);

			fs = 10;
			testCase.signal3 = device.mockSignal(testCase.len, [], fs);

			samplesPerDay = fs * 60 * 60 * 24;
			testCase.days4 = 5;
			testCase.signal4 = device.mockSignal(testCase.days4 * samplesPerDay, ...
												 nAxes, fs);
		end
	end

	methods (Test, TestTags = {'Unit'})
		function testSingleAxis(testCase)
			time = testCase.signal1.getTime;
			testCase.verifyLength(time, length(testCase.signal1))
			testCase.verifyEqual(time(1), 0)
			testCase.verifyEqual(time(end), ((testCase.len) / testCase.signal1.fs) - ...
								 (1 / testCase.signal1.fs))
		end

		function testMultipleAxes(testCase)
			time = testCase.signal2.getTime;
			testCase.verifyLength(time, length(testCase.signal2))
			testCase.verifyEqual(time(1), 0)
			testCase.verifyEqual(time(end), ((testCase.len) / testCase.signal2.fs) - ...
								 (1 / testCase.signal2.fs))
		end

		function testDifferentFs(testCase)
			time = testCase.signal3.getTime;
			endTime = (testCase.len / testCase.signal3.fs) - (1 / testCase.signal3.fs);
			testCase.verifyLength(testCase.signal3, length(time))
			testCase.verifyEqual(0, time(1))
			testCase.verifyEqual(time(end), endTime)
		end

		function testMultipleDays(testCase)
			testCase.signal4.reshape('days');
			expectedSize = testCase.signal4.size;
			timeDim = testCase.signal4.dim('time');
			dayDim = testCase.signal4.dim('days');
			expectedSize = expectedSize([timeDim, dayDim]);

			time = testCase.signal4.getTime;
			testCase.verifySize(time, expectedSize);
			secondsPerDay = 3600 * 24;
			testCase.verifyEqual(time(1, 2), secondsPerDay)
		end
	end

	methods (Test, TestTags = {'Performance'})
	end
end
