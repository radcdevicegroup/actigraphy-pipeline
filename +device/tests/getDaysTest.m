classdef getDaysTest < matlab.unittest.TestCase

	properties
		exactNumDays = 1.9;
		sig;
	end

	methods (TestMethodSetup)
		function setupSignal(testCase)
			fs = 1 / 100;
			samplesPerDay = fs * 3600 * 24;
			testCase.sig = device.mockSignal(...
				floor(samplesPerDay * testCase.exactNumDays), ...
				1, fs);
		end
	end

	methods (Test, TestTags = {'Unit'})
		function testSignalNumDays(testCase)
			testCase.verifyEqual(floor(testCase.exactNumDays), ...
								 testCase.sig.getNumDays);
		end
	end

end
