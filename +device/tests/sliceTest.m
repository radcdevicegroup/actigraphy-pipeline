classdef sliceTest < matlab.unittest.TestCase

	properties
		index
		signal1
		signal2
		record1
		record2
	end

	methods (TestMethodSetup)
		function setupTest(testCase)
			testCase.index = 10:50;
			testCase.signal1 = device.mockSignal(100);
			testCase.signal1.slice(testCase.index);
			testCase.signal2 = device.mockSignal(100, 3);
			testCase.signal2.slice(testCase.index);

			fs = 1;
			testCase.record1 = device.mockRecord(100, [], fs);
			testCase.record2 = device.mockRecord(100, 3, fs);
		end
	end

	methods (Test, TestTags = {'Unit'})
		function testSingleAxisSignal(testCase)
			testCase.verifyEqual(testCase.signal1.data, testCase.index')
		end

		function testMultiAxisSignal(testCase)
			testCase.verifyEqual(testCase.signal2.data, repmat(testCase.index', [1, 3]))
		end

		function testSingleSignalRecord(testCase)
			times = testCase.record1.signals.signal1.getTime + ...
					convertTo(testCase.record1.startTime, 'posix');
			idx1 = times(testCase.index(1));
			idx2 = times(testCase.index(end));
			testCase.record1.slice(idx1, idx2);

			testCase.verifyEqual(testCase.record1.signals.signal1.data, testCase.index');
		end

		function testMultiSignalRecord(testCase)
			times = testCase.record2.signals.signal1.getTime + ...
					convertTo(testCase.record2.startTime, 'posix');
			idx1 = times(testCase.index(1));
			idx2 = times(testCase.index(end));
			testCase.record2.slice(idx1, idx2);

			testCase.verifyEqual(testCase.record2.signals.signal1.data, testCase.index');
			testCase.verifyEqual(testCase.record2.signals.signal2.data, testCase.index');
			testCase.verifyEqual(testCase.record2.signals.signal3.data, testCase.index');
		end
	end
end
