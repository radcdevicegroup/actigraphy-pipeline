classdef mockRecordTest < matlab.unittest.TestCase

	properties
		len = 100
		nAxes = 3
		nSignals = 4
		fs = 1
		record1
		record2
		record3
		record4
	end

	methods (TestMethodSetup)
		function setup(testCase)
			testCase.record1 = device.mockRecord(testCase.len);
			testCase.record2 = device.mockRecord([testCase.len testCase.nAxes]);
			testCase.record3 = device.mockRecord([testCase.len testCase.nAxes], ...
											  testCase.nSignals);
			testCase.record4 = device.mockRecord(testCase.len, testCase.nSignals, ...
											  testCase.fs);
		end
	end

	methods (Test, TestTags = {'Unit'})
		function testNames(testCase)
			testCase.verifyTrue(testCase.record1.hasSignal('signal1'))
			names = {'signal1', 'signal2', 'signal3', 'signal4'}';
			testCase.verifyEqual(testCase.record3.listSignals, names)
			testCase.verifyEqual(testCase.record4.listSignals, names)
		end

		function testShape(testCase)
			testCase.verifyEqual(length(testCase.record1.signals.signal1), testCase.len);
			testCase.verifyEqual(length(testCase.record2.signals.signal1), testCase.len);
			testCase.verifyEqual(length(testCase.record3.signals.signal1), testCase.len);
			testCase.verifyEqual(length(testCase.record3.signals.signal2), testCase.len);
			testCase.verifyEqual(length(testCase.record4.signals.signal1), testCase.len);
			testCase.verifyEqual(length(testCase.record4.signals.signal3), testCase.len);

			testCase.verifyEqual(size(testCase.record2.signals.signal1), ...
										[testCase.len testCase.nAxes]);
			testCase.verifyEqual(size(testCase.record3.signals.signal1), ...
										[testCase.len testCase.nAxes]);
			testCase.verifyEqual(size(testCase.record3.signals.signal3), ...
										[testCase.len testCase.nAxes]);
		end

		function testNumberAxes(testCase)
			testCase.verifyEqual(testCase.record1.signals.signal1.nAxes, 1);
			testCase.verifyEqual(testCase.record2.signals.signal1.nAxes, testCase.nAxes);
			testCase.verifyEqual(testCase.record3.signals.signal1.nAxes, testCase.nAxes);
			testCase.verifyEqual(testCase.record3.signals.signal4.nAxes, testCase.nAxes);
			testCase.verifyEqual(testCase.record4.signals.signal1.nAxes, 1);
			testCase.verifyEqual(testCase.record4.signals.signal3.nAxes, 1);
		end

		function testNumberSignals(testCase)
			testCase.verifyEqual(length(testCase.record1.listSignals), 1);
			testCase.verifyEqual(length(testCase.record2.listSignals), 1);
			testCase.verifyEqual(length(testCase.record3.listSignals), testCase.nSignals);
			testCase.verifyEqual(length(testCase.record4.listSignals), testCase.nSignals);
		end

		function testSampleFreq(testCase)
			testCase.verifyEqual(testCase.record1.signals.signal1.fs, 0.01)
			testCase.verifyEqual(testCase.record2.signals.signal1.fs, 0.01)
			testCase.verifyEqual(testCase.record3.signals.signal1.fs, 0.01)
			testCase.verifyEqual(testCase.record3.signals.signal2.fs, 0.01)
			testCase.verifyEqual(testCase.record4.signals.signal1.fs, testCase.fs)
			testCase.verifyEqual(testCase.record4.signals.signal3.fs, testCase.fs)
		end
	end
end
