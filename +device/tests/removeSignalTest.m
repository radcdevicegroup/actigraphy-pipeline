classdef removeSignalTest < matlab.unittest.TestCase & matlab.perftest.TestCase

	properties
		record
		signalList
	end

	methods (TestMethodSetup)
		function setupRemoveTests(testCase)
			testCase.record = device.mockRecord(10, 10);
			testCase.signalList = testCase.record.listSignals();
		end
	end

	methods (Test, TestTags = {'Unit'})
		function testRemove(testCase)
			testCase.record.removeSignal('signal1');
			testCase.verifyEqual(testCase.record.listSignals(), testCase.signalList(2:end))
		end

		function testMultipleRemove(testCase)
			testCase.record.removeSignal(testCase.signalList([1, 3, 8]));
			expected = testCase.signalList([2, 4, 5, 6, 7, 9, 10]);
			testCase.verifyEqual(testCase.record.listSignals(), expected)
		end

		function testNonExistantFields(testCase)
			testCase.record.removeSignal(testCase.signalList([1, 3, 8]));
			expected = testCase.signalList([2, 4, 5, 6, 7, 9, 10]);
			testCase.record.removeSignal(testCase.signalList(1));
			testCase.verifyEqual(testCase.record.listSignals(), expected)
			testCase.record.removeSignal(testCase.signalList([1, 3, 8]));
			testCase.verifyEqual(testCase.record.listSignals(), expected)
		end

		function testOtherInputs(testCase)
			testCase.record.removeSignal(testCase.signalList([1, 3, 8]), 'signal2');
			expected = testCase.signalList([4, 5, 6, 7, 9, 10]);
			testCase.verifyEqual(testCase.record.listSignals(), expected)
		end
	end

	methods (Test, TestTags = {'Performance'})
	end
end
