classdef reshapeToDaysTest < matlab.unittest.TestCase & matlab.perftest.TestCase

    properties
        samplesPerDay
        numDays
        signal1
        signal2
        perfsignal1
        perfsignal2
        perfNumDays
        record1
        record2
    end

    methods (TestMethodSetup)
        function setupReshape(testCase)
            fs = 1 / 300;
            testCase.samplesPerDay = 24 * 3600 * fs;
            testCase.numDays = 3;
            testCase.signal1 = device.mockSignal(...
                testCase.samplesPerDay * testCase.numDays, ...
                [], fs);

            testCase.signal2 = device.mockSignal(...
                testCase.samplesPerDay * testCase.numDays, ...
                3, fs);

            testCase.signal1.reshape;
            testCase.signal2.reshape;

            perfSize = 1000000;
            testCase.perfsignal1 = device.mockSignal(perfSize, [], fs);
            testCase.perfNumDays = floor(length(testCase.perfsignal1) / ...
                                         testCase.samplesPerDay);
            testCase.perfsignal2 = device.mockSignal(perfSize, 10, fs);

            testCase.record1 = device.mockRecord(...
                testCase.samplesPerDay * testCase.numDays, [], fs);

            testCase.record2 = device.mockRecord(...
                testCase.samplesPerDay * testCase.numDays, 3, fs);
        end
    end

    methods (Test, TestTags = {'Unit'})
        function testShape(testCase)
            testCase.verifyEqual(testCase.signal1.size, ...
                                 [testCase.samplesPerDay testCase.numDays])

            testCase.verifyEqual(testCase.signal2.size, ...
                                 [testCase.samplesPerDay testCase.signal2.nAxes testCase.numDays])
        end

        function testValues(testCase)
            testCase.verifyEqual(testCase.signal1.data(1, :), ...
                                 1 + (testCase.samplesPerDay * (0:2)))

            testCase.verifyEqual(testCase.signal1.data(end, :), ...
                                 testCase.samplesPerDay * (1:3))

            testCase.verifyEqual(testCase.signal1.data(:, 1), ...
                                 (1:testCase.samplesPerDay)')

            testCase.verifyEqual(testCase.signal2.data(1, :, 1), ...
                                 1 + (testCase.samplesPerDay * (0:2)))

            testCase.verifyEqual(testCase.signal2.data(end, :, 2), ...
                                 testCase.samplesPerDay * (1:3))

            testCase.verifyEqual(testCase.signal2.data(:, 1, 3), ...
                                 (1:testCase.samplesPerDay)')
        end

        function testRecord(testCase)
            testCase.record1.reshapeToDays;
            testCase.verifyEqual(size(testCase.record1.signals.signal1), ...
                                 [testCase.samplesPerDay, testCase.numDays - 1])

            record3 = testCase.record2.copy;
            record4 = testCase.record2.copy;

            testCase.record2.reshapeToDays;
            testCase.verifyEqual(size(testCase.record2.signals.signal1), ...
                                 [testCase.samplesPerDay, testCase.numDays - 1])
            testCase.verifyEqual(size(testCase.record2.signals.signal2), ...
                                 [testCase.samplesPerDay, testCase.numDays - 1])
            testCase.verifyEqual(size(testCase.record2.signals.signal3), ...
                                 [testCase.samplesPerDay, testCase.numDays - 1])

            record3.reshapeToDays(15, {'signal1', 'signal2', 'signal3', 'notASignal'});
            testCase.verifyEqual(size(record3.signals.signal1), ...
                                 [testCase.samplesPerDay, testCase.numDays - 1])
            testCase.verifyEqual(size(record3.signals.signal2), ...
                                 [testCase.samplesPerDay, testCase.numDays - 1])
            testCase.verifyEqual(size(record3.signals.signal3), ...
                                 [testCase.samplesPerDay, testCase.numDays - 1])

            record4.reshapeToDays([], 'signal1', 'notASignal','signal3');
            testCase.verifyEqual(size(record4.signals.signal1), ...
                                 [testCase.samplesPerDay, testCase.numDays - 1])
            testCase.verifyNotEqual(size(record4.signals.signal2), ...
                                    [testCase.samplesPerDay, testCase.numDays - 1])
            testCase.verifyEqual(size(record4.signals.signal3), ...
                                 [testCase.samplesPerDay, testCase.numDays - 1])
        end

        function testEquivalentReshapes(testCase)
            testCase.verifyEqual(customReshape(testCase, testCase.perfsignal1), ...
                                 reshape(testCase.perfsignal1.data(...
                                     1:(testCase.perfNumDays * testCase.samplesPerDay), :), ...
                                         [testCase.samplesPerDay, ...
                                testCase.perfNumDays, ...
                                testCase.perfsignal1.nAxes]))

            testCase.verifyEqual(customReshape(testCase, testCase.perfsignal2), ...
                                 reshape(testCase.perfsignal2.data(...
                                     1:(testCase.perfNumDays * testCase.samplesPerDay), :, :), ...
                                         [testCase.samplesPerDay, ...
                                testCase.perfNumDays, ...
                                testCase.perfsignal2.nAxes]))
        end
    end

    methods
        function data = customReshape(testCase, signal)
            data = signal.data;

            testCase.startMeasuring();
            dataHold = data;
            indeces = ((0:(testCase.perfNumDays - 1)) * testCase.samplesPerDay) + ...
                      (1:testCase.samplesPerDay)';

            data = zeros(testCase.samplesPerDay, ...
                         testCase.perfNumDays, ...
                         size(dataHold, 2));
            for ax = 1:signal.nAxes
                tmp = dataHold(:, ax);
                data(:, :, ax) = tmp(indeces);
            end
            testCase.stopMeasuring();
        end
    end

    methods (Test, TestTags = {'Performance'})
        function testCustomReshapeSingleAxis(testCase)
            customReshape(testCase, testCase.perfsignal1)
        end

        function testBuiltinReshapeSingleAxis(testCase)
            data = testCase.perfsignal1.data;

            testCase.startMeasuring();
            data = reshape(data(1:(testCase.perfNumDays * testCase.samplesPerDay), :), ...
                           [testCase.samplesPerDay testCase.perfNumDays 1]);
            testCase.stopMeasuring();
        end

        function testCustomReshapeMultiAxes(testCase)
            customReshape(testCase, testCase.perfsignal2)
        end

        function testBuiltinReshapeMultiAxes(testCase)
            data = testCase.perfsignal2.data;

            testCase.startMeasuring();
            data = reshape(data(1:(testCase.perfNumDays * testCase.samplesPerDay), :, :), ...
                           [testCase.samplesPerDay testCase.perfNumDays testCase.perfsignal2.nAxes]);
            testCase.stopMeasuring();
		end
	end
end
