function record = load(filename)
%LOAD read a saved record
%   RECORD = LOAD(FILENAME) load the RECORD stored in FILENAME.
%
%   See also RECORD, DEVICE/SAVE, SAVE, LOAD.

    record = load(filename);
    record = record.obj;
end
