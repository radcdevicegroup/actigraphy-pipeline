# \+device: abstract data structures to bring devices into the matrix

**Table of Contents**

- [Summary](#summary)
- [Record:](#record)
    - [Properties](#properties)
    - [Methods](#methods)
- [Signal:](#signal)
    - [Properties](#properties-1)
    - [Methods](#methods-1)
- [Additional functions:](#additional-functions)

## Summary

The device package introduces two classes: Record and Signal. The Record
class stores information collected from a device—the recorded signals
along with data about the device and the record itself (i.e. time
started and ended). In addition, the class provides common methods for
processing and manipulating records, such as shaping the recorded into
days and removing days, and saving results either by printing their
variables are storing the record to a file.

Signals are stored as Signal objects. The signal class adds basic data
structure for storing the signal's time series data, sampling frequency,
and number of axes as well as methods for working with those signals.
Most (if not all) of the work on a Signal object is handle by the Record
class and so it shouldn't be necessary to know much about what the
Signal class does.

This package is intended to be abstract, it does not provide device
specific functionality. For that, other packages should be written to
integrate device types (see [+pipeline
package](https://bitbucket.org/radcdevicegroup/pipeline/src/master/) for
example).

Below summarizes the packages features. For more details see the help
for the classes and individual methods.

## Record:

### Properties

`keys`: Unique identifying data about the record.

This is a structure with fields: projId, fuYear, and deviceId. Each of
these values are numbers pinpointing a the record. No two records should
have the same values for each of these fields. When combined the keys
can be used find a record in different data files or to join two tables.

`info`: Additional information about the signal.

Another structure providing information about the record beyond what's
needed to identify it. Includes start and end of the recording, where
the device was placed on the participant, the length of the recording in
days (after removing days based on quality control), and the maximum
consecutive days in the record (longest streak of days not removed by
quality control).

`deviceType`: The name of the device used to create the recording.

`signals`: A structure of signals as signal objects.

`variables`: A structure of variables organized into variable sets.

Each field in `variables` is the name of a variable set. Variable sets
are a group of variables calculated under a specific circumstances.
Usually, this is based on the signals required to calculate the
variables but more complected logic can be used like including the
placement of the device and taking into account the length of the
recording. For example all variables calculated with the accelerometer
from a wrist mounted device may go into one variable set but variables
calculated with both the accelerometer and gyroscope will be placed into
a separate variable set. This assures as long as a device is wrist
mounted and has an accelerometer the resulting signals can be compared
to other devices with those signals even if some devices also have a
gyroscope.

`startOfDay`: The start of the day in hours \[0, 24) or `NONE`.

Used to trim recordings to an exact number of days. All data before
`startOfDay` on the first day and all data after `startOfDay` on the
last day are removed from the recording. For recordings with less than
one day, `startOfDay` can be set to `NONE` which will prevent the record
from being trimmed and otherwise expected to be a multiday recording.

### Methods

`save`: Save the record to the given file name.

A wrapper around the normal `save` function.

`copy`: Make a deep copy of the record.

The Record class is a handle type meaning it gets passed by reference
instead of passed by copy like most MATLAB data types. To prevent an
analysis from affecting modifying a record make a copy before passing it
to a function. For many functions this should not be necessary and not
coping it can reduce memory in some cases (although MATLAB is pretty
good at not coping objects unless they actually need to be).

`hasSignal`: Test if record holds onto a signal with the given name.

`addSignal`: Add a new signal

Can either pass data to the Signal constructor or accept an already
created signal. Useful when creating a new signal out of an already
existing signal.

`removeSignal`: Delete a signal or list of signals by name.

`listSignals`: Return a list of the signals currently held by the
record.

`getSignal`: Get a Signal object from the record's list of signals or a
property of a signal.

`mapSignal`: Run a function over each of a records signal's.

`slice`:Remove data from outside of a specified time range.

`trim`: Remove data from before the start of the first full day and
after the end of the last full day.

`reshapeToDays`: Reshape the signals into columns of individual days.

`getNumDays`: Length of the recording in days. `setDaysWorn`: Remove
days based on a days worn index.

Used during construction of the record when the constructor is passed a
quality control function. Is probably best not to use this directly as
it may lead to incorrect values in the `info` property. With the
exception of throw-away cases, such as if using to calculate variables
on different subsets of the data.

`addVariable`: Set a new variable in a variable set.

`removeVariable`: Remove a variable from a variable set.

`listVariables`: List a variable set's variables.

`listVariableSets`: List a record's variable sets.

`print`: write a variable set to a csv.

## Signal:

### Properties

`data`: Time series data with each column an individual axis and time
going down the columns.

`fs`: Sampling frequency.

`nAxes`: Number of axes.

Determined by the number of columns in the original data. Once a signal
has been reshaped the number of axes will no longer be the same as the
number of columns.

### Methods

`Size`: Alias for `size(obj.data)`.

`length`: Number of time points.

The length time series data down the columns. This could be different
than the normal `length(Signal.data)` which returns
`max(size(Signal.data))` but it's unlikely the number of rows won't be
much greater than the number of columns so they should generally be
equivalent.

`slice`: Alias for `obj.data(indices, :)` for cutting in time.

`getTime`: Calculate a relative time vector—first value is always
0—using the sampling frequency.

`getNumDays`: Return the number of full days the signal was recorded
over.

`isReshaped`: Test if the signal has been reshaped.

`reshape`: reshape the signal into days or hours.

`dim`: Return the number of the named dimension.

The position of the physical dimensions changes when the data is
reshaped. This allows dimension to be asked for by name (i.e. 'time'
which will always be 1 or 'axes' which can change).

`norm`: Calculate the euclidean norm across all axes.

`copy`: Make a deep copy of the signal.

## Additional functions:

`mockRecord`: Create a fake Record object (for testing purposes).

`mockSignal`: Create a fake Signal object.

`load`: Load a Record object from the given file name.

`map`: Run a function over all records in a directory or list of
records.
