function preread(fileDir)
%PREREAD batch convert accelerometer files to mat for reduce load time
%   PREREAD(DIR) reads all files in DIR and saves them as a Record object
%   then deletes the original file. This will reduce load times for running
%   manual QC functions.
%
%   The resulting mat files will also generally be smaller than the original
%   files as they only save what is needed for a record and are compressed well.
%
%   Warning: This will delete files.

    device.map(@convertFile, fileDir, 'debug', true);

    function convertFile(filename)
        pipeline.read(filename, 'qcFunc', 'None', 'save', true);
        if ~endsWith(filename, '.mat')
            delete(filename);
        end
    end
end
