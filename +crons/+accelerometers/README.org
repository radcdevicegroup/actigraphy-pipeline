* Wrist-worn activity monitor scripts
This package provides some helper functions for running wrist-worn activity monitor pipelines.
To run the pipeline normally the ~pipeline.run(dataDir, outDir)~ will work like any other pipeline.
If a manual qc function is preferred over an automated qc function the files in this package provide a way to manually run a QC before running through the pipeline.

First files should be ~preread~ to convert the bin or cwa files to a record stored in mat files.
This will reduce the loading time between records during the QC process.
Next the invoke ~manualQcFunc~ on the same directory and un-tick any days that should not be included in the analysis.
After performing the QC the files can be passed to ~pipeline.run~ like with the parameter ~'qcFunc'~ set to ~'None'~ prevent the automatic QC function from further processing the files.

The full process will look like:

#+BEGIN_SRC matlab :eval no :exports code
dataDir = appendToDataPath('geneactiv/server');
outDir = appendToOutPath('pipeline');

accelerometer.preread(dataDir);
accelerometer.manualQcFunc(dataDir);
pipeline.run(dataDir, outDir, 'qcFunc', 'None');
#+END_SRC

Where ~dataDir~ and ~outDir~ should be changed to the correct paths.
In practice, the above should be split into scheduled tasks.
The ~manualQcFunc~ is the only part that would be run at the MATLAB REPL.
Pre-reading should be performed after syncing the data directory with the server.
And ~pipeline.run~ would be placed in a scheduled task to be run after the ~manualQcFunc~.
For example, a server syncing and pre-reading script could be run every Sunday, the QC would be manually performed sometime during the week, then a second script running the pipeline would be scheduled every Friday night.
In the above example, if a QC was not performed during the week the pipeline would be run on the full---not QC'd---files.
To fix this an intermediary directory could be used to sync with the server and QC'd files would be moved to the true data directory.
In this case, if a week was missed there would be no new files in the data directory for the pipeline to run and two weeks worth of files to work on in the following week.
