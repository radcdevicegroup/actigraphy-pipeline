%% Help
% Runs the pipeline over watchPAT data directory. Use after syncing watchPAT
% directory with server to update the output files.

files = utils.listFiles(appendToDataPath('watchpat/server/*.edf'));
outdir = appendToOutPath('watchpat');

pipeline.run(files, outdir, 'delim', '|');
