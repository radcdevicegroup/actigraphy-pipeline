#!/usr/bin/env bash

cd $(dirname "$0")

rsync dconnell@radc.rush.edu:watchpat_from_datamgt ~/data/watchpat/
matlab -nodisplay -r "watchpat; exit" > /dev/null
rsync ~/out/watchpat/pipeline_pulse.csv dconnell@radc.rush.edu:pipeline_pulse.csv
