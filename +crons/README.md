# Cron scripts

A set of MATLAB and Bash scripts to update the output files when new
data files have been uploaded to the server. Bash scripts call the
MATLAB scripts so only the Bash scripts need to be run. The scripts
assume the locations of files, their names, and the server user's login.
If this is to be run on a computer with values that differ from those
expectations alter the Bash scripts as needed.

## Dependencies

Depends on the MATLAB packages
[+pipeline](https://bitbucket.org/radcdevicegroup/pipeline/src/master/)
and [+utils](https://bitbucket.org/radcdevicegroup/utils/src/master/)
and expects the functions `appendToDataPath` and `appendToOutPath` have
been defined to get an absolute file path relative to the computer's
data and out directories respectively (see [pipeline's getting
started](https://radcdevicegroup.bitbucket.io/pipeline/start/) for an
example).

## Scheduling

The Bash scripts can be added to a crontab to be automatically run on a
schedule. Because the scripts sync with server—which requires the user's
password to be typed at the keyboard—to use with a crontab server keys
need to be setup first.

To add to a crontab run `crontab -e` at a shell repl to edit the crontab
and add the path to the file and when to run it (see [Crontab – Quick
Reference](https://www.adminschoice.com/crontab-quick-reference)). This
can also be run with other methods of running scheduled tasks so long as
the computer can run Bash. Otherwise, the Bash scripts will need to be
replaced with a substitute applicable to the computer running the tasks
(possibly powershell for windows).
