function [r2, model, yhat] = regression(x, y)
%REGRESSION calculate linear regression from x to y
%   R2 = REGRESSION(X, Y) calculate coefficient of correlation for Ys
%   dependency on X variables. X should be a matrix of column vector variables.
%
%   [R2, MODEL] = REGRESSION(X, Y) return model paramaeters such that X *
%   MODEL predicts Y. Where X has had a column of ones concatenated to it in
%   for the bias term (X = [ONES(LENGTH(X), 1) X]).
%
%   [R2, MODEL, YHAT] = REGRESSION(X, Y) return the esitmation of Y, X * MODEL.

    x = [ones(length(x), 1) x];
    model = x \ y;

    yhat = x * model;
    residuals = sum((y - yhat) .^ 2) / (length(y) - 2);
    total = sum((y - mean(y)) .^ 2) / (length(y) - 1);
    r2 = 1 - (residuals / total);
end
