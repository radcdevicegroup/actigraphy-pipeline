#ifndef __LOWESS_H_
#define __LOWESS_H_

#include <mex.h>

void ones(double *mat, size_t n);
uint fix(double val);
bool is_uniform(double *x, size_t n);
void distance(double *x, double *d, size_t j, size_t n);
double abs_difference(double a, double b);
void lowess(double *x, double *y, double f, double *yhat, size_t n);
double rwlreg(double *x, double *y, size_t n, double d, double *r, double xx);
double max(double a, double b);
void wlsq(double *x, double *y, double *w, size_t n, double *a);
double median(double *x, size_t n);

#endif // __LOWESS_H_
