/* LOWESS- Locally Weighted Scatterplot Smoothing
 *
 * Takes a column vector of the independent variables X and an equal length
 * column vector of the dependent variable Y along with a parameter f between 0
 * and 1 to determine the relative range of surronding variables to consider.
 *
 * Values of f near 1 lead to smoother regressions while fs near 0 take into
 * account more high frequency components.
 *
 * X is expected to be sorted and the y(i) should be a function of x(i).
 * The algorithm is more efficient if X has constant step size
 * (i.e. if x(i + 1) * - x(i) = C for all i).
 */

#include "lowess.h"

void mexFunction(int nlhs, mxArray *plhs[], int nrhs, const mxArray *prhs[]) {
  double *x;
  double *y;
  double f;
  size_t nx, ny, mx, my;
  double *yhat;

  if (nrhs != 3)
    mexErrMsgIdAndTxt("Lowess:nrhs", "Requires three inputs");

  if (nlhs != 1)
    mexErrMsgIdAndTxt("Lowess:nlhs", "Too many outputs; expects one output");

  mx = mxGetM(prhs[0]);
  my = mxGetM(prhs[1]);
  nx = mxGetN(prhs[0]);
  ny = mxGetN(prhs[1]);

  if (mx != my)
    mexErrMsgIdAndTxt("Lowess:size", "x and y must have same length.");

  if (nx != 1 || ny != 1)
    mexErrMsgIdAndTxt("Lowess:size", "x and y must be column vectors.");

  if (!mxIsDouble(prhs[0]) || !mxIsDouble(prhs[1]))
    mexErrMsgIdAndTxt("Lowess:notDouble", "First two inputs must be doubles.");

  if (!mxIsDouble(prhs[2]) || mxGetNumberOfElements(prhs[2]) != 1)
    mexErrMsgIdAndTxt("Lowess:notScaler", "Last input must be scaler.");

#if MX_HAS_INTERLEAVED_COMPLEX
  x = mxGetDoubles(prhs[0]);
  y = mxGetDoubles(prhs[1]);
#else
  x = mxGetPr(prhs[0]);
  y = mxGetPr(prhs[1]);
#endif

  f = mxGetScalar(prhs[2]);
  plhs[0] = mxCreateDoubleMatrix((mwSize)mx, 1, mxREAL);

#if MX_HAS_INTERLEAVED_COMPLEX
  yhat = mxGetDoubles(plhs[0]);
#else
  yhat = mxGetPr(plhs[0]);
#endif

  lowess(x, y, f, yhat, mx);
}

int cmpfunc(const void *a, const void *b) {
  return (*(double *)a > *(double *)b);
}

void lowess(double *x, double *y, double f, double *yhat, size_t n) {
  uint m = fix((n * f) + 0.5);
  double window[n];
  double d[n];
  double r[n];

  ones(r, n);

  if (is_uniform(x, n)) {
    distance(x, d, 0, n);
    for (size_t j = 0; j < n; j++) {
      /* Normally would index the sorted distance vector for j at m - 1.
       * Assuming a constant step between x values this distance vector
       * changes in a predictable fashion with j: (0 s 2s 3s ...), (0 s s 2s
       * 3s ...), (0 s s 2s 2s 3s ...), .... Additionally, the distance vector
       * for j = 0 is already sorted since x is sorted. The below indexing is
       * equivalent to indexing the sorted distance(x, d, j, n) vector for
       * each j at m - 1.*/
      if (j < (m / 2)) {
        window[j] = d[m - 1 - j];
      } else if (j > (n - (m / 2))) {
        window[j] = d[(m - 1) - ((n - 1) - j)];
      } else {
        window[j] = d[m / 2];
      }
      yhat[j] = rwlreg(x, y, n, window[j], r, x[j]);
    }
  } else {
    for (size_t j = 0; j < n; j++) {
      distance(x, d, j, n);
      qsort(d, n, sizeof(double), cmpfunc);
      window[j] = d[m - 1];
      yhat[j] = rwlreg(x, y, n, window[j], r, x[j]);
    }
  }

  double s;
  for (size_t i = 0; i < 2; i++) {
    for (size_t j = 0; j < n; j++) {
      r[j] = abs_difference(y[j], yhat[j]);
    }

    s = median(r, n);

    for (size_t j = 0; j < n; j++) {
      r[j] /= (6 * s);
      r[j] = 1 - (r[j] * r[j]);
      r[j] = max(0., r[j]);
      r[j] *= r[j];
    }

    for (size_t j = 0; j < n; j++) {
      yhat[j] = rwlreg(x, y, n, window[j], r, x[j]);
    }
  }
}

void ones(double *vec, size_t n) {
  /* Fill *mat with ones. */
  for (size_t j = 0; j < n; j++) {
    vec[j] = 1.;
  }
}

uint fix(double val) {
  /* Round VAL towords zero.
   * Ex: fix(1.7) = 1; fix(-1.7) = -1; */
  return (uint)val;
}

bool is_uniform(double *x, size_t n) {
  double step_size = x[1] - x[0];
  for (size_t j = 1; j < (n - 1); j++) {
    if ((x[j + 1] - x[j]) != step_size) {
      return 0;
    }
  }
  return 1;
}

void distance(double *x, double *d, size_t j, size_t n) {
  /* Distance between the jth index of array *x and all other points of *x.
   * Distance (as opposed to difference) implies values must be positive.*/
  for (size_t i = 0; i < n; i++) {
    d[i] = abs_difference(x[i], x[j]);
  }
}

double abs_difference(double a, double b) {
  /* Return the absolute value of the difference between A and B. */
  if (a > b) {
    return (a - b);
  } else {
    return (b - a);
  }
}
double rwlreg(double *x, double *y, size_t n, double d, double *r, double xx) {
  double dd = d;
  double ddmax = abs_difference(x[n - 1], x[0]);
  double f;
  double w[n];
  double c = 0;
  double total = 0;

  if (dd == 0.) {
    mexErrMsgIdAndTxt("Lowess:windowsize",
                      "LOWESS window size = 0. Increase f.");
  } else {
    while (dd <= ddmax) {
      total = 0;
      c = 0;
      for (size_t j = 0; j < n; j++) {
        f = abs_difference(x[j], xx) / dd;
        f = 1. - (f * f * f);
        f = max(0., f);
        w[j] = f * f * f * r[j];
        total += w[j];
        if (w[j] > 0) {
          c += w[j];
        }
      }

      if (c > 3) {
        break;
      } else {
        dd *= 1.28;
      }
    }
  }

  for (size_t j = 0; j < n; j++) {
    w[j] /= total;
  }

  double a[2];
  wlsq(x, y, w, n, a);
  return a[0] + a[1] * xx;
}

double max(double a, double b) {
  if (a > b) {
    return a;
  } else {
    return b;
  }
}

void wlsq(double *x, double *y, double *w, size_t n, double *a) {
  double wx = 0, wy = 0, wxx = 0, wxy = 0;
  double tmpx, tmpy;
  for (size_t j = 0; j < n; j++) {
    tmpx = w[j] * x[j];
    tmpy = w[j] * y[j];
    wx += tmpx;
    wy += tmpy;
    wxx += tmpx * x[j];
    wxy += tmpy * x[j];
  }

  a[1] = (wxy - (wy * wx)) / (wxx - (wx * wx));
  a[0] = wy - (a[1] * wx);
}

double median(double *x, size_t n) {
  double tmp[n];
  for (size_t i = 0; i < n; i++) {
    tmp[i] = x[i];
  }

  qsort(tmp, (size_t)n, sizeof(double), cmpfunc);
  if ((n % 2) == 0) {
    return (tmp[(n / 2) - 1] + tmp[(n / 2)]) / 2.;
  } else {
    return (tmp[((n - 1) / 2)]);
  }
}
