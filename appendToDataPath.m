function newPath = appendToDataPath(addendum)
%APPENDTODATAPATH get absolute path from path relative to data path
%   ABSPATH = APPENDTODATAPATH(RELPATH) converts RELPATH, relative to the
%   data path, to an absolute path.

	newPath = utils.pathjoin(getenv('HOME'), 'data', addendum);
end